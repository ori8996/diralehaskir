const constant = {
    status: {
        waitingForApproval: "1",
        approve: "2",
        disApprove: "3",
        closed: "4"
    },
    statusDisplay: {
        1: {
            class: "waitingForApproval",
            display: "מחכה לאישור"
        },
        2: {
            class: "approve",
            display: "אושרה"
        },
        3: {
            class: "disApprove",
            display: "לא אושרה"
        },
        4: {
            class: "closed",
            display: "הושכרה"
        }
    }
}

export default constant;