const theme = {
    Button: {
        titleStyle: {
            color: '#732F72',
            fontSize: 18,
            fontWeight: 'bold'
        },
        containerStyle: {
            marginTop: 5,
            width:"50%",
            alignSelf: 'center'
        },
        buttonStyle:{
            backgroundColor: 'white',
            justifyContent: 'center',
            borderRadius: 15,
            borderWidth: 2,
            borderColor: '#4d263c',
        }
    },
    viewStyle:{
        flexDirection: 'row',
        justifyContent:"flex-end",
        alignSelf: 'flex-end',
        alignItems: 'flex-end',
        marginBottom:"2%"
    },
    viewStyleSec:{
        flexDirection: 'row',
        justifyContent:"flex-end",
        alignSelf: 'flex-end',
        alignItems: 'flex-end',
        marginTop:20,
    },
    checkBoxStyle:{
        width:"33%",
        backgroundColor:"transperent",
        borderWidth:0
    },
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    detailsView:{
        flexDirection:'row',
        justifyContent: 'center',
        marginTop: "2%"
    },
    stepper:{
        flexDirection:'row-reverse',
        alignSelf: 'center',
        marginTop: 10,
        width: '90%'
    },
    apertView:{
        flexDirection:'row',
        alignSelf: 'flex-end',
        marginTop: 10,
    },
    title:{
        marginTop:10,
        fontSize: 32,
        alignSelf: 'center',
        color:"#4d263c",
        fontWeight:"bold"
    },
    loginTitle:{
        marginBottom:20,
        fontSize: 55,
        alignSelf: 'center',
        color:"#4d263c",
        fontWeight:"bold"
    },
    waitingForApproval:{
        color: 'black',
        fontWeight: "bold"
    },
    approve:{
        color: 'green',
        fontWeight: "bold",
        justifyContent: 'center'
    },
    disApprove:{
        color: 'red',
        fontWeight: "bold",
        justifyContent: 'center'
    },
    gnrtView:{
        flexDirection:'row',
        justifyContent: 'center',
        marginTop:-20
    }
  };

  export default theme;