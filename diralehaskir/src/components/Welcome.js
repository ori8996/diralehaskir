import React, {Component} from 'react';
import {ImageBackground, View, BackHandler} from 'react-native';
import {withNavigation} from 'react-navigation';
import {Button,Card,ThemeProvider} from 'react-native-elements';
import theme from '../themes/mainTheme';
import { Icon } from 'react-native-elements';

class Welcome extends Component {

  componentDidMount() { 
    BackHandler.addEventListener('hardwareBackPress', () => {return true});
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', () => {return true});
  }
  render() {
    return (<ImageBackground
      source={require('../../images/welcome1.png')}
      style={{width: '100%', height: '100%'}}>
        <ThemeProvider theme={theme}>
            <View style={{top:"90%",flexDirection:"row",justifyContent:"center"}}>
            <Card  containerStyle={{backgroundColor:"hsla(311, 100%, 18%, 0.8)",borderWidth:0}} width={"40%"} height={"90%"}>
                <Icon
                    name='home'
                    color="white"
                    type='font-awesome'
                    size={110}/>
                <Button containerStyle={{width:"100%",marginTop:-10}} 
                        buttonStyle={{backgroundColor:"transperent",borderWidth:0}} 
                        titleStyle={{color:"white",fontSize:22,fontWeight:"bold"}} 
                        title="מצא דירה"
                        onPress={() => this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "basicQuestions" }})}></Button>
            </Card>
            <Card containerStyle={{backgroundColor:"hsla(311, 100%, 18%, 0.8)",borderWidth:0}} width={"40%"} height={"90%"}>
                <View style={{flexDirection:"row",justifyContent:"center"}}>
                    <Icon
                        name='plus'
                        color="white"
                        type='antdesign'
                        size={35}/>
                    <Icon
                        name='home'
                        color="white"
                        type='antdesign'
                        size={100}/>
                </View>
                <Button containerStyle={{width:"100%"}} 
                        buttonStyle={{backgroundColor:"transperent",borderWidth:0}} 
                        titleStyle={{color:"white",fontSize:22,fontWeight:"bold"}} 
                        title="הוסף דירה"
                        onPress={() => this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "addApartment" }})}></Button>
            </Card>
            </View>
        </ThemeProvider>
      </ImageBackground>)
  }
}

export default withNavigation(Welcome);
