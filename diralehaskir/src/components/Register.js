import React, {Component} from 'react';
import {ImageBackground,Text,Image,View } from 'react-native';
import firebase from 'firebase';
import 'firebase/firestore';
import {Spinner} from './common';
import {withNavigation} from 'react-navigation';
import { SCLAlert, SCLAlertButton } from 'react-native-scl-alert';
import {ThemeProvider, Header } from 'react-native-elements';
import { Hoshi } from 'react-native-textinput-effects';
import theme from '../themes/mainTheme';
import AwesomeButton from "react-native-really-awesome-button";


class Register extends Component {
  state = {
    email: '',
    password: '',
    error: '',
    loading: false,
    loggedIn: null,
    visible: false,
  };

  onButtonPress() {
    const {email, password} = this.state;

    this.setState({error: '', loading: true});

    firebase.auth().createUserWithEmailAndPassword(email, password).then(cred => { 
      firebase.firestore().collection('users').doc(cred.user.uid).set({
        apartment_prefernces:{},
        isAdmin:false
      }).then(this.onRegisterSuccess.bind(this));
    })
    .catch(() => {
      this.onRegisterFail();
    });
  }

  onRegisterFail() {
    console.log('onRegisterFail');
    let errorStr;
    if (!this.state.email) {
      errorStr = 'הכנס אימייל';
    } else if (this.state.password.length < 6) {
      errorStr = 'סיסמא באורך 6 תווים לפחות';
    } else {
      errorStr = 'אימייל לא תקין';
    }
    this.setState({
      error: errorStr,
      loading: false,
    });
  }

  onRegisterSuccess() {
    console.log('onRegisterSuccess');
    this.setState({
      email: '',
      password: '',
      loading: false,
      error: '',
      visible: true,
    });
  }

  renderButton() {
    if (this.state.loading) {
      return <Spinner size="small" />;
    }
    return(<AwesomeButton style={{marginTop: 5,width:"50%",alignSelf: 'center'}} 
                                  width="100%" backgroundColor={"white"} textColor={"#732F72"} 
                                  borderRadius={15} textSize={20}
                                  onPress={this.onButtonPress.bind(this)}>הרשם</AwesomeButton>);
  }

  render() {
    return (
      <ImageBackground
      source={require('../../images/login.png')}
      style={{width: '100%', height: '100%',position:"absolute"}}>
        <ThemeProvider theme={theme}>
          <Header backgroundColor="transperent" containerStyle={{height: '15%'}}
              centerComponent={<Image source={require('../../images/diralehaskir.png')} style={{ width: 140, height: 100 }}></Image>}
              rightComponent={{ icon: 'arrow-forward', color: 'black',onPress:() => this.props.navigation.navigate('master', { screen: 'Home' }) }}
            />
          <View style={{marginTop:50}}>
            <Text style={theme.loginTitle}>הרשמה</Text>
              <View>
                <Hoshi
                  label={'Email'}
                  labelStyle={{color:"black"}}
                  style={{borderBottomColor:"black",borderBottomWidth:2}} 
                  inputStyle={{color:"black"}}
                  borderColor={'#b76c94'}
                  borderHeight={2}
                  inputPadding={16}
                  value={this.state.email}
                  onChangeText={email => this.setState({email})}
                />
              </View>
              <View style={{marginTop:10}}>
                <Hoshi
                  secureTextEntry
                  label={'password'}
                  labelStyle={{color:"black"}}
                  style={{borderBottomColor:"black",borderBottomWidth:2}} 
                  inputStyle={{color:"black"}}
                  borderColor={'#b76c94'}
                  borderHeight={2}
                  inputPadding={16}        
                  value={this.state.password}
                  onChangeText={password => this.setState({password})}
                />
              </View>

              <Text style={theme.errorTextStyle}>{this.state.error}</Text>
              {this.renderButton()}
              <SCLAlert
                  theme="success"
                  show={this.state.visible}
                  title="נרשמת בהצלחה!"
                  onRequestClose={() => {this.setState({visible:false}); this.props.navigation.navigate('master', { screen: 'Login' })}}
                  >
                  <Text style={{fontSize:20,marginTop:-50}}></Text>
                  <SCLAlertButton theme="success" onPress={() => {this.setState({visible:false}); this.props.navigation.navigate('master', { screen: 'Login' })}}>
                      כניסה לאתר
                  </SCLAlertButton>
              </SCLAlert>
          </View>
        </ThemeProvider>
     </ImageBackground>);
	}
}

export default withNavigation(Register);
