import React, {Component} from 'react';
import {ImageBackground,Text, View,Image} from 'react-native';
import firebase from 'firebase';
import {Spinner} from './common';
import {withNavigation} from 'react-navigation';
import {ThemeProvider, Header } from 'react-native-elements';
import theme from '../themes/mainTheme';
import { Hoshi } from 'react-native-textinput-effects';
import AwesomeButton from "react-native-really-awesome-button";


class Login extends Component {
  state = {email: '', password: '', error: '', loading: false, loggedIn: null,isNew:0};
  
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.checkIfNew();
        this.setState({loggedIn: true});
      } else {
        this.setState({loggedIn: false});
      }
    });
  }

  onButtonPress() {
    const {email, password} = this.state;

    this.setState({error: '', loading: true});

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(this.onLoginSuccess.bind(this))
      .catch(() => {
        console.log('fail');
        this.onLoginFail();
      });
  }

  checkIfNew(){
    this.userId = firebase.auth().currentUser.uid;
    firebase.firestore().collection("users").doc(this.userId).get().then((querySnapshot) => {
      if(Object.keys(querySnapshot.data().apartment_prefernces).length != 0 || querySnapshot.data().isAdmin){
        this.setState({isNew: 1});
      } else{
        this.setState({isNew: 2});
      }
    });
  }

  onLoginFail() {
    this.setState({
      error: 'שם המשתמש או הסיסמא לא נכונים',
      loading: false,
    });
  }

  onLoginSuccess() {
    this.checkIfNew();
    this.setState({
      email: '',
      password: '',
      loading: false,
      error: '',
    });
  }

  renderButton() {
    if (this.state.loading) {
      return <Spinner size="small" />;
    }
    return(<AwesomeButton style={{marginTop: 5,width:"50%",alignSelf: 'center'}} 
                                  width="100%" backgroundColor={"white"} textColor={"#732F72"} 
                                  borderRadius={15} textSize={20}
                                  onPress={this.onButtonPress.bind(this)}>התחבר</AwesomeButton>);
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
        switch (this.state.isNew) {
          case 1:
            return(this.props.navigation.navigate('master', { screen: 'Main' }))
          case 2:
            return(this.props.navigation.navigate('master', { screen: 'Welcome' }))
          default:
            return <Spinner size="large" />;
        }
      case false:
        return (
          <View style={{marginTop:50}}>
            <Text style={theme.loginTitle}>התחברות</Text>
            <View>
                <Hoshi
                  label={'Email'}
                  labelStyle={{color:"black"}}
                  style={{borderBottomColor:"black",borderBottomWidth:2}} 
                  inputStyle={{color:"black"}}
                  borderColor={'#b76c94'}
                  borderHeight={3}
                  inputPadding={16}
                  value={this.state.email}
                  onChangeText={email => this.setState({email})}
                />
              </View>
              <View style={{marginTop:10}}>
                <Hoshi
                  secureTextEntry
                  label={'password'}
                  labelStyle={{color:"black"}}
                  style={{borderBottomColor:"black",borderBottomWidth:2}}
                  inputStyle={{color:"black"}}   
                  borderColor={'#b76c94'}
                  borderHeight={3}
                  inputPadding={16}        
                  value={this.state.password}
                  onChangeText={password => this.setState({password})}
                />
              </View>
            <Text style={theme.errorTextStyle}>{this.state.error}</Text>

            {this.renderButton()}
          </View>
        );
      default:
        return <Spinner size="large" />;
    }
  }

  render() {
    return (<ImageBackground
      source={require('../../images/login.png')}
      style={{width: '100%', height: '100%'}}>
        <ThemeProvider theme={theme}>
          <Header backgroundColor="transperent" containerStyle={{height: '15%'}}
                centerComponent={<Image source={require('../../images/diralehaskir.png')} style={{ width: 140, height: 100 }}></Image>}
                rightComponent={{ icon: 'arrow-forward', color: 'black',onPress:() => this.props.navigation.navigate('master', { screen: 'Home' }) }}
              />
          {this.renderContent()}
        </ThemeProvider>
      </ImageBackground>)
  }
}

export default withNavigation(Login);
