import React, {Component} from 'react';
import firebase from 'firebase';
import {Spinner} from './common';
import {
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
  FlatList,
  Text,
} from 'react-native';

import {Button} from 'react-native-elements';
import {withNavigation} from 'react-navigation';
import GenApart from './GenApart';
import { SCLAlert, SCLAlertButton } from 'react-native-scl-alert';

let ratings = {};
class GenerateApartments extends Component {
  state = {dbData:{},apartments: [], moreApartments:[], loading: true, loadedApart:5, fetching_from_server: false, visible: true, isUnderTwo: false};
  neighborhoods = {}
  
  componentDidMount(){
    const db = firebase.firestore();
    let userId = firebase.auth().currentUser.uid;
    db.collection("users").doc(userId).get().then((querySnapshot) => {
      if(Object.keys(querySnapshot.data().apartment_prefernces).length != 0){
        let userData = {apartment_prefernces : querySnapshot.data().apartment_prefernces };
        userData.userId = userId;
        userData.apartment_prefernces.neighborhood = userData.apartment_prefernces.neighborhood.path;
        this.setState({dbData: userData});
        this.getApartments(userId);
      }
    });
  }

  getApartments(){
    const db = firebase.firestore();
    var that = this;
    db.collection("neighborhoods").get().then(function(querySnapshot) {
      querySnapshot.forEach(doc => {
        that.neighborhoods[doc.ref.path]=doc.data().name;
      });

      let index = that.state.apartments.length + 1;
      console.log(that.state.dbData);
      fetch('https://us-central1-diralehaskir.cloudfunctions.net/generateApartments', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(that.state.dbData)
        }).then((response) =>{ response.json().then((responseJson) => {
          for(let apart of responseJson){
            apart.apartment.id = index;
            apart.apartment.neighborhood = that.neighborhoods[apart.apartment.neighborhood];
            ratings[index] = {...apart.modelData, tag:-1};
            index++;
          }
          that.setState({apartments: responseJson,loading: false})
        })});
    });

    
  }

  updateRating(apartmentId, newRate){
    let value = {};
    switch(newRate){
      case 0:
        value = {location: ratings[apartmentId].location,
                 price: ratings[apartmentId].price,
                 quality: ratings[apartmentId].quality,
                 tag: 0};
        break;
      case 2:
        value = {location: ratings[apartmentId].location,
                 price: ratings[apartmentId].price,
                 quality: ratings[apartmentId].quality,
                 tag: 1};
        break;
      default:
        break;
    }

    if(Object.keys(value).length != 0){
      ratings[apartmentId] = value;
    }
  }

  sendRating(){
    let userId = firebase.auth().currentUser.uid;
    let finalRating = [];
    let countPlus = 0;
    let countMinus = 0;
    for(let rated of Object.values(ratings)){
      if(rated.tag != -1){
        finalRating.push(rated);
        countPlus += (rated.tag == 1) ? 1 : 0;
        countMinus += (rated.tag == 0) ? 1 : 0;
      }
    }

    if(countPlus < 2 || countMinus < 2){
      this.setState({isUnderTwo: true}); 
    } else {
      fetch('https://us-central1-diralehaskir.cloudfunctions.net/buildModel?userId=' + userId, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(finalRating)
      }).then((response) =>{
        response.json().then((res)=>{
          console.log(res);
        });
      });
  
      return(this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "apartments", first: true }}));
    }
  }
 
  loadMoreData = () =>
  {
    let index = this.state.apartments.length + 1;        
    this.setState({ fetching_from_server: true }, () =>
      {
        setTimeout(() =>
          {
            fetch('https://us-central1-diralehaskir.cloudfunctions.net/generateApartments', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(this.state.dbData)
            }).then((response) =>{
              response.json().then((responseJson) =>{
                for(let apart of responseJson){
                  console.log(index);
                  apart.apartment.id = index;
                  apart.apartment.neighborhood = this.neighborhoods[apart.apartment.neighborhood];
                  ratings[index] = {...apart.modelData, tag:-1};
                  index++;
                }
                this.setState({ apartments: [ ...this.state.apartments, ...responseJson ], fetching_from_server: false })
              }); 
            }) 
          }, 500); 
      });
  }

  getTitle(){
    let title = "";
    switch(this.state.apartments.length){
      case 7:
        title = "פרגן עוד כמה דירוגים, זה בשבילך";
        break;
      case 14: 
        title = "מעולה, אתה תמיד יכול עוד אם באלך, ככה תמצא את הדירה המושלמת";
        break;
      case 21:
        title = "אוקיי אוקיי זה מספיק";
        break;
      default:
        title = "השקעת"
    }

    return title;
  }

  renderFooter()
  {
    let title = this.getTitle();
    if (this.state.fetching_from_server) {
      return (<Spinner size="large"/>);
    } else 
    return (
        <View>
          <Text style={{textAlign:"center",fontSize:26,fontWeight:"bold",marginBottom:10}}>{title}</Text>
          <Button containerStyle={{marginBottom:25,width:"30%"}} 
                  buttonStyle ={{borderWidth:0}}
                  titleStyle={{color:"black"}}
                  title="טען עוד"
                  onPress= {this.loadMoreData}></Button>                   
        </View>
    )
  }

  render(){
    if (this.state.loading || this.state.apartments.length == 0) {
      return (<Spinner size="large"/>);
    }
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        <FlatList
        data={this.state.apartments} 
        renderItem={(item,index) => {
            return(<GenApart updateRate={this.updateRating.bind(this)} apartment={item.item.apartment}></GenApart>)
        }}
        keyExtractor = {( item, index ) => index }
        ListFooterComponent = { this.renderFooter.bind( this ) }/>
          <Button containerStyle={{marginBottom:25}} 
                  title="המשך"
                  onPress= {this.sendRating.bind(this)}></Button>
          <SCLAlert
            overlayStyle={{}}
            theme="info"
            show={this.state.visible}
            title="דף דירות פקטיביות">
              <Text style={{fontSize:18,marginTop:-40}}>בדף זה, המערכת שלנו עיבדה את הנתונים שהזנת, ויצרה דירות פקטיביות (דירות לא אמיתיות). בשלב זה עליך לעבור על הדירות ולבחור האם הדירה מצאה חן בעניך או לא, ואם אינך בטוח ניתן גם לא לדרג. בעזרת שלב זה נוכל ללמוד את רצונותיך בצורה הטובה ביותר!                 (מומלץ לדרג כמה שניתן בשביל התאמה גבוה)</Text>
            <SCLAlertButton theme="info" onPress={() => {this.setState({visible:false});}}>
                הבנתי
            </SCLAlertButton>
          </SCLAlert>
          <SCLAlert
            overlayStyle={{}}
            theme="info"
            show={this.state.isUnderTwo}
            title="עליך לדרג יותר">
              <Text style={{fontSize:18,marginTop:-40}}>כדי שנלמד אותך בצורה ביותר, אנחנו צריכים שתדרג לפחות 2 דירות שאהבת ו2 דירות שלא</Text>
            <SCLAlertButton theme="info" onPress={() => {this.setState({isUnderTwo:false});}}>
                חזור
            </SCLAlertButton>
          </SCLAlert>
      </View>
    );
  }
    
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scrollView: {
    flex: 1,
    padding: 10,
    paddingTop: 0,
  },
});

export default withNavigation(GenerateApartments);
