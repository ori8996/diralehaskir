import React, {Component} from 'react';
import {ImageBackground, View} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {ThemeProvider } from 'react-native-elements';
import theme from '../themes/mainTheme';
import AwesomeButton from "react-native-really-awesome-button";

function Home({navigation}) {
  return (
    <ImageBackground
      source={require('../../images/home.png')}
      style={{flex: 1,
        resizeMode: "cover",
        justifyContent: "center"}}>
      <View style={styles.containerStyle}>
        <ThemeProvider theme={theme}>
          <AwesomeButton style={{marginTop: 5,width:"50%",alignSelf: 'center'}} 
                         width="100%" backgroundColor={"white"} textColor={"#732F72"} 
                         borderRadius={15} textSize={20}
                         onPress={() => navigation.navigate('master', { screen: 'Login' })}>התחברות</AwesomeButton>
          <AwesomeButton style={{marginTop: 5,width:"50%",alignSelf: 'center'}} 
                         width="100%" backgroundColor={"#732F72"} textColor={"white"} 
                         borderRadius={15} textSize={20}
                         onPress={() => navigation.navigate('master', { screen: 'Register' })}>הרשמה</AwesomeButton>
        </ThemeProvider>
      </View>
    </ImageBackground>
  );
}

const styles = EStyleSheet.create({
  containerStyle: {
    top: "11%",
  }
});

export default Home;
