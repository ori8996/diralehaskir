import React, {
  Component,
} from 'react';

import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import { CheckBox } from 'react-native-elements';
import theme from '../../themes/mainTheme';
import { Icon } from 'react-native-elements'

export default class LowerCard extends Component {
  state = { loading: false,userData: {ac: false, accessible: false, balcony: false, elevator: false, equipped_kitchen: false,
    furnished: false, parking: false, partners: false, pets: false, shelter: false,
    storeroom: false, longTerm: false, price:"", rooms:"", contact:""}};

  render() {
    const onPress = this.props.onPress;
    const apart = this.props.apart;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff',
          borderWidth: 8,
          borderRadius: 5,
          borderColor:"white"
        }}>
          <View style={{ flex: 1 }}>
            <View>
                <View style={{flexDirection:'row',
                              justifyContent: 'center',marginTop:-10}}>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='air-conditioner' type='material-community'/>}
                    uncheckedIcon={<Icon
                      name='air-conditioner' type='material-community' color='#C6C6AE'/>}
                    title="מזגן"
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.ac}/>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='accessible' type='material'/>}
                    uncheckedIcon={<Icon
                      name='accessible' type='material' color='#C6C6AE'/>}
                    title="נגישות"
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.accessible}/>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='garage-open' type='material-community' size={25}/>}
                    uncheckedIcon={<Icon
                      name='garage-open' type='material-community' color='#C6C6AE' size={25}/>}
                    title="מרפסת"
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.balcony}/>
                </View>
                <View style={theme.gnrtView}>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='elevator' type='foundation'/>}
                    uncheckedIcon={<Icon
                      name='elevator' type='foundation' color='#C6C6AE'/>}
                    title="מעלית"
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.elevator}/>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='bars' type='font-awesome'/>}
                    uncheckedIcon={<Icon
                      name='bars' type='font-awesome' color='#C6C6AE'/>}
                    title="סורגים"
                    textStyle={{marginRight:15}}
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.bars}/>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='sofa' type='material-community'/>}
                    uncheckedIcon={<Icon
                      name='sofa' type='material-community' color='#C6C6AE'/>}
                    title="מרוהטת"
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.furnished}/>
                </View>
                <View style={theme.gnrtView}>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    title="חנייה"
                    checkedIcon={<Icon
                      name='parking' type='material-community'/>}
                    uncheckedIcon={<Icon
                      name='parking' type='material-community' color='#C6C6AE'/>}
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.parking}/>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='people' type='material'/>}
                    uncheckedIcon={<Icon
                      name='people' type='material' color='#C6C6AE'/>}
                    title="שותפים"
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.partners}/>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='dog-side' type='material-community'/>}
                    uncheckedIcon={<Icon
                      name='dog-side' type='material-community' color='#C6C6AE'/>}
                    title="בעלי חיים"
                    textStyle={{marginRight:10}}
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.pets}/>
                </View>
                <View style={theme.gnrtView}>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='box' type='feather'/>}
                    uncheckedIcon={<Icon
                      name='box' type='feather' color='#C6C6AE'/>}
                    title="מקלט"
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.shelter}/>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='garage' type='material-community'/>}
                    uncheckedIcon={<Icon
                      name='garage' type='material-community' color='#C6C6AE'/>}
                    title="מחסן"
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.storeroom}/>
                    <CheckBox
                    disabled
                    iconRight
                    right
                    checkedIcon={<Icon
                      name='file' type='octicon'/>}
                    uncheckedIcon={<Icon
                      name='file' type='octicon' color='#C6C6AE'/>}
                    textStyle={{marginRight:15}}
                    title="טווח ארוך"
                    containerStyle={{width:100,backgroundColor:"#fff",borderColor:"#fff"}}
                    checked={apart.longTerm}/>
                </View>  
            </View>
          </View>
          <TouchableOpacity
            style={{alignItems: "center",backgroundColor: '#EFC368',fontSize:18,borderRadius:5,borderWidth:1}}
            onPress={onPress}
          >
            <Text style={{color: 'black',fontSize:18}}>סגור</Text>
          </TouchableOpacity>
        </View>
    );
  }
}
