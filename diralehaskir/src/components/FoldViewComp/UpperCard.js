import React, {
  Component,
} from 'react';

import {
  View,
  StyleSheet,
  Text
} from 'react-native';

import {Button,ButtonGroup,Image } from 'react-native-elements';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'row',
    borderWidth: 8,
    borderRadius: 5,
    borderColor:'#4d263c'
  },
  rightPane: {
    flex: 1,
    backgroundColor: '#4d263c',
    padding: 16,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  leftPane: {
    flex: 2,
    padding: 16,
    backgroundColor: '#fff',
  },
});

export default class LowerCard extends Component {
  state = {selectedIndex: 1,loading:true};
  updateIndex = this.updateIndex.bind(this);
  homeTypes = ['דירה','דירת גן','דופלקס','קוטג','פנטהאוז'];
  
  component1 = () => <Image source={require('../../../images/unlike.png')} style={{ width: 35, height: 35 }} />;
  component2 = () => <Image source={require('../../../images/neutral.png')} style={{ width: 35, height: 35 }} />;
  component3 = () => <Image source={require('../../../images/like.png')} style={{ width: 35, height: 35 }} />;

  buttons = [{ element: this.component1 }, { element: this.component2 },{ element: this.component3 }];

  updateIndex (selectedIndex) {
    this.setState({selectedIndex});
    this.props.updateRate(this.props.apart.id,selectedIndex);   
  }  

  renderRight(){
    const onPress = this.props.onPress;
    if(this.props.flip){
      return(<View>
        <Text style={{color:"white"}}>סגור כדי לדרג</Text>
      </View>);
    } else{
      return(<View>
        <ButtonGroup
            onPress={this.updateIndex}
            selectedIndex={this.state.selectedIndex}
            buttons={this.buttons}
            containerStyle={{height: "40%",width:"120%",marginLeft:-5}} 
            selectedButtonStyle={{backgroundColor:"#FFD700"}}/>
          <Button containerStyle={{width:"90%"}} titleStyle= {{color: 'black'}} buttonStyle={{borderRadius:5,backgroundColor:"#EFC368",borderColor: 'black'}} onPress={onPress} title="הרחב" />
      </View>);
    }
  }

  // let selectedIndex = 1;
  render() {
    const onPress = this.props.onPress;
    const apart = this.props.apart;
    return (
      <View style={styles.container}>
        <View style={styles.leftPane}>
          <View style={{ flex: 1, flexDirection: 'column',justifyContent:"center" }}>
            <View style={{ flexDirection: 'row-reverse'}}>
              <Text style={{fontSize:22,fontWeight:"bold"}}>שכונה:  </Text>
              <Text style={{fontSize:22}}>{apart.neighborhood}</Text>
            </View>
            <View style={{ flexDirection: 'row-reverse', marginTop:10}}>
              <Text style={{fontSize:22,fontWeight:"bold"}}>גודל:  </Text>
              <Text style={{fontSize:22}}>{apart.size} מ"ר</Text>
            </View>
            <View style={{ flexDirection: 'row-reverse', marginTop:10}}>
              <Text style={{fontSize:22,fontWeight:"bold"}}>מספר חדרים:  </Text>
              <Text style={{fontSize:22}}>{apart.rooms}</Text>
            </View>
            <View style={{ flexDirection: 'row-reverse', marginTop:10}}>
              <Text style={{fontSize:22,fontWeight:"bold"}}>מחיר:  </Text>
              <Text style={{fontSize:22}}>₪{apart.price}</Text>
            </View>
          </View>
        </View>
        <View style={styles.rightPane}>
          <Text style={{color:"white",fontSize:24}}>{this.homeTypes[apart.homeType-1]}</Text>

          {this.renderRight()}
        </View>

      </View>
    );
  }
}
