import React, {Component} from 'react';
import { Text, View, Dimensions, Image } from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';

const { width: screenWidth } = Dimensions.get('window')

class ExplanationPage extends Component {

    constructor(props){
        super(props);
        this.state = {
          activeIndex:0,
          carouselItems: [
          {
            title:"מילוי פרטים אישיים",
            text: "בדף זה נרצה לדעת איזה דירה אתה מחפש, ולכן תסמן כל מה שחשוב לך ונשתדל למצוא דירות כמה שיותר מתאימות",
            image: require('../../images/basicQuestions.jpg')
          },
          {
            title:"בחירת זמן חיפוש",
            text: "בדף זה נרצה לדעת כמה זמן יש לך לחיפושי הדירה, אתה יכול לבחור כל תאריך עתידי ונשתדל למצוא לך דירה עד אותו תאריך",
            image: require('../../images/timeSelection.jpg')
          }
        ]
      }
    }

    _renderItem({item,index}){
        return (
            <View style={{
                borderRadius: 5,
                height: 500,
                padding: 30
              }}>
                <Image style={{ width: 250, height: 300}} source={item.image} ></Image>
                <Text style={{ marginTop:5, lineHeight:30, fontSize: 15, fontStyle:"italic"}}>{item.text}</Text>
            </View>
        )
    }

    get pagination () {
        const { carouselItems, activeIndex } = this.state;
        return (
            <Pagination
              dotsLength={carouselItems.length}
              activeDotIndex={activeIndex}
              containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }}
              dotStyle={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  marginHorizontal: 8,
                  backgroundColor: 'rgba(255, 255, 255, 0.92)'
              }}
              inactiveDotStyle={{
                backgroundColor: "#c71b1b"
              }}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
            />
        );
    }
   
    render() {
        return (
            <View>
                <Carousel
                  layout={"default"}
                  ref={ref => this.carousel = ref}
                  data={this.state.carouselItems}
                  sliderWidth={screenWidth}
                  sliderHeight={screenWidth}
                  itemWidth={screenWidth - 60}
                  renderItem={this._renderItem}
                  onSnapToItem = { index => this.setState({activeIndex:index}) }/>
                  { this.pagination }
            </View>
        );
    }
}

export default ExplanationPage;