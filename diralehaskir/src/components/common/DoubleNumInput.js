import React, { Component } from "react";  
import {StyleSheet, View, TextInput, Text } from "react-native";  

class DoubleNumInput extends Component {
  state = {firstNumber: '', secondNumber:''};

  componentDidMount(){
    if(this.props.min){
      this.setState({ firstNumber: this.props.min.toString()});
    }

    if(this.props.max){
      this.setState({ secondNumber: this.props.max.toString()});
    }
  }

  onchangeFirstText(text){
    if(!isNaN(text)) {
      let firstNum = Number(text);
      this.props.onChangeFirstText(firstNum);
      this.setState({ firstNumber: text});
    }
  }

  onchangeSecondText(text){
    if(!isNaN(text)) {
      let secondNum = Number(text);
      this.props.onChangeSecondText(secondNum);
      this.setState({ secondNumber: text});
    }
  }

  onFirstEndEditing(){
    if(this.state.secondNumber != '' && this.state.firstNumber > this.state.secondNumber ) {
      this.onchangeSecondText(this.state.firstNumber);
    }
  }

  onSecondEndEditing(){
    if(this.state.firstNumber != '' && this.state.secondNumber < this.state.firstNumber) {
      this.onchangeFirstText(this.state.secondNumber);
    }
  }

  render() {
    const {containerStyle, labelStyle, TextInputStyle, firstLabelStyle, secondLabelStyle} = styles;
    return (  
      <View style={containerStyle}>
        <Text style={labelStyle}>{this.props.label + ":"}</Text>
        <Text style={firstLabelStyle}>{" מ-"}</Text>
        <TextInput
          maxLength={10}
          value={this.state.firstNumber}
          underlineColorAndroid='transparent'  
          style={TextInputStyle}
          onChangeText={firstNumber => this.onchangeFirstText(firstNumber)}
          onEndEditing={() => this.onFirstEndEditing()}
          keyboardType={'numeric'}/>
        <Text style={secondLabelStyle}>{"עד-"}</Text>
        <TextInput
          value={this.state.secondNumber}
          underlineColorAndroid='transparent'  
          style={TextInputStyle}
          onChangeText={(secondNumber) => this.onchangeSecondText(secondNumber)}
          onEndEditing={() => this.onSecondEndEditing()}
          keyboardType={'numeric'}/>
      </View>  
    );  
  }  
}  

const styles = StyleSheet.create({
  TextInputStyle: {
    backgroundColor:'white',
    fontSize: 16,
    width: "30%"
  },
  containerStyle: {
    flexDirection: 'row-reverse',
    width: '90%',
    alignSelf: 'center'
  },
  firstLabelStyle: {
    fontSize: 18,
    paddingLeft: 10,
    paddingTop: 12
  },
  labelStyle: {
    fontSize: 18,
    paddingLeft: 10,
    paddingTop: 12,
    width:'15%'
  },
  secondLabelStyle: {
    fontSize: 18,
    paddingLeft: 10,
    paddingTop: 12,
    marginRight: 10
  }
});

export {DoubleNumInput};