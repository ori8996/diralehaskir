import NumericInput from 'react-native-numeric-input';
import React, { Component } from "react";  
import {View, Text} from 'react-native';

class Stepper extends Component {
  state = {firstNumber: this.props.firstValue, secondNumber:this.props.secondValue}

  onchangeFirst(num){
    this.props.onFirstChange(num);
    this.setState({ firstNumber: num});
    if(this.state.firstNumber > this.state.secondNumber ) {
      this.onchangeSecond(this.state.firstNumber);
    }
  }

  onchangeSecond(num){
    this.props.onSecondChange(num);
    this.setState({ secondNumber: num});
    if(this.state.firstNumber > this.state.secondNumber ) {
      this.onchangeFirst(this.state.secondNumber);
    }
  }

  render() {
    const {containerStyle, labelStyle} = styles;
    return ( 
      <View style={containerStyle}>
          <Text style={labelStyle}>{this.props.firstLabel}</Text>
          <NumericInput 
          editable={false}
          minValue={this.props.minValue}
          maxValue={this.props.maxValue}
          step={this.props.step}
          valueType='real'
          value={this.state.firstNumber}
          onChange={(firstNumber) => this.onchangeFirst(firstNumber)}/>
          <Text style={labelStyle}>{this.props.secondLabel}</Text>
          <NumericInput 
          editable={false}
          minValue={this.props.minValue}
          maxValue={this.props.maxValue}
          step={this.props.step}
          valueType='real'
          value={this.state.secondNumber}
          onChange={(secondNumber) => this.onchangeSecond(secondNumber)}/>
      </View>
    );
  }
}

const styles = {
    labelStyle: {
        fontSize: 18,
        paddingLeft: 5,
        paddingTop: 12,
        marginRight: 5
    },
    containerStyle: {
        flexDirection: 'row-reverse',
        alignSelf: 'flex-end',
        width:'60%'
    }
};

export {Stepper};
