import React, { Component } from "react";  
import {StyleSheet, View, Picker, Text } from "react-native";  

class DoublePicker extends Component {
    
    state = {firstSelected:this.props.firstValue.toString(), secondSelected:this.props.secondValue.toString()}
  
    dataList = () => {
        return( this.props.data.map( (x,i) => { 
            return( <Picker.Item label={x} key={i} value={x}  />)
        } ));
    }

    onChangeFirst(value){
        if(this.props.number){
            this.props.onChangeFirst(Number(value));
        } else {
            this.props.onChangeFirst(value);
        }

        this.setState({firstSelected : value});
        if(value > this.state.secondSelected) {
            this.onChangeSecond(value);
        }
    }

    onChangeSecond(value){
        if(this.props.number){
            this.props.onChangeSecond(Number(value));
        } else {
            this.props.onChangeSecond(value);            
        }

        this.setState({secondSelected : value});
        if(value < this.state.firstSelected) {
            this.onChangeFirst(value);
        }
    }

  render() {
    const {containerStyle, labelStyle, PickerStyle, firstLabelStyle, secondLabelStyle} = styles;
    return (  
      <View style={containerStyle}>
        <Text style={labelStyle}>{this.props.label + ":"}</Text>
        <Text style={firstLabelStyle}>{" מ-"}</Text>
        <Picker
            style= {PickerStyle}
            selectedValue={this.state.firstSelected}
            onValueChange={ (value) =>  this.onChangeFirst(value) }>
            { this.dataList() }
        </Picker>
        <Text style={secondLabelStyle}>{"עד-"}</Text>
        <Picker
            style= {PickerStyle}
            selectedValue={this.state.secondSelected}
            onValueChange={ (value) => this.onChangeSecond(value)}>
            { this.dataList() }
        </Picker>
      </View>  
    );  
  }  
}  

const styles = StyleSheet.create({
  PickerStyle: {
    backgroundColor:'white',
    fontSize: 16,
    width: "30%",
  },
  containerStyle: {
    flexDirection: 'row-reverse',
    width: '90%',
    alignSelf: 'center'
  },
  
  firstLabelStyle: {
    fontSize: 18,
    paddingLeft: 10,
    paddingTop: 12
  },
  labelStyle: {
    fontSize: 18,
    paddingLeft: 10,
    paddingTop: 12,
    width:'18%'
  },
  secondLabelStyle: {
    fontSize: 18,
    paddingLeft: 10,
    paddingTop: 12,
    marginRight: 10
  }
});

export {DoublePicker};