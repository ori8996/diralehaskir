import React, { Component } from "react";  
import {View, Text} from 'react-native';
import { CheckBox } from 'react-native-elements';

class MultiplePicker extends Component {
  state = {
    checkboxes: [],
    dataChoosed: []
  };

  componentDidMount() {
    let tempCheckBoxes = [];
    let id = 1;
    this.props.data.forEach(val => tempCheckBoxes.push({id: id++, title: val, checked: false}));
    this.setState({checkboxes: tempCheckBoxes});
  }

  toggleCheckbox(id) {
    
    const changedCheckbox = this.state.checkboxes.find((cb) => cb.id === id);
  
    changedCheckbox.checked = !changedCheckbox.checked;
  
    let checkboxes = this.state.checkboxes;
    checkboxes[changedCheckbox.id - 1] = changedCheckbox;
    this.setState({checkboxes});

    let newDataChoosed = this.state.dataChoosed;
    if(changedCheckbox.checked) {
      newDataChoosed.push(changedCheckbox.title);
      this.setState({ dataChoosed: newDataChoosed});
    } else {
      newDataChoosed = newDataChoosed.filter(item => item !== changedCheckbox.title);
      this.setState({ dataChoosed: newDataChoosed});
    }
    this.props.onPress(newDataChoosed);
  }

  renderCheckBoxes(){
    return (
      this.state.checkboxes.map((cb) => {
        return (
          <CheckBox
            containerStyle={styles.checkBoxStyle}
            key={cb.id}
            title={cb.title}
            checked={cb.checked}
            onPress={() => this.toggleCheckbox(cb.id)} />
        )
      })
    );
  }

  render() {
    const {containerStyle, labelStyle} = styles;
    return (
      <View style={containerStyle}>
          <Text style={labelStyle}>{this.props.label}</Text>
          {this.renderCheckBoxes()}
      </View>
    );
  }
}

const styles = {
    labelStyle: {
        fontSize: 18,
        paddingLeft: 5,
        paddingTop: 12,
        marginRight: 5
    },
    containerStyle: {
        flexDirection: 'row-reverse',
        alignSelf: 'flex-end',
        width:'90%'
    },
    checkBoxStyle: {
      marginTop: 10
    }
};

export {MultiplePicker};
