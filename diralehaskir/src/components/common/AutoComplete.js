import Autocomplete from 'react-native-autocomplete-input';
import React, {Component} from 'react';
import {StyleSheet,Text, View,Image,TouchableOpacity} from 'react-native';

class AutoComplete extends Component {
  state = {query:this.props.value ,press: false};
  
  find(query, data) {
    if (query === '' || this.state.press) {
      return [];
    }

    return data.filter((datum => datum.value.startsWith(query)));
  }

  onPress(item){
    this.props.onChange(item);
    this.setState({ query: item.value, press: true })
  }

  onchange(text){
    this.props.onChange('');
    this.setState({ query: text, press: false })
  }

  render() {
    const {containerStyle, labelStyle, autocompleteContainer} = styles;
    const { query } = this.state;
    const currData = this.find(query, this.props.data);
    return (
      <View style={containerStyle}>
        <Text style={{fontSize: 18, paddingTop: 10, marginLeft:"3%",marginRight:10, color:this.props.textColor}}>{this.props.label}:</Text>
        <Autocomplete
          style={autocompleteContainer}
          inputContainerStyle={{borderWidth: 0}}
          listStyle={autocompleteContainer}
          data={currData}
          onChangeText={text => this.onchange(text)}
          defaultValue={query}
          renderItem={({ item, i }) => (
            <TouchableOpacity onPress={() => this.onPress(item)}>
              <Text style={{color: 'black',fontSize: 16}}>{item.value}</Text>
            </TouchableOpacity>
          )}/>
        </View>
      ); 
  }
 }

const styles = StyleSheet.create({
    autocompleteContainer: {
      borderBottomWidth: 2,
      borderColor: 'black',
      fontSize: 16,
      textAlign:"right",
      color: 'black',
      fontSize: 18,
      fontWeight: 'bold'
    },
    containerStyle: {
      flexDirection: 'row-reverse',
      width:"95%",
      alignSelf: 'flex-end',
    },
    labelStyle: {
      fontSize: 18,
      paddingTop: 10,
      marginLeft:"1%",
      color:"black"
    }
  });

export {AutoComplete};