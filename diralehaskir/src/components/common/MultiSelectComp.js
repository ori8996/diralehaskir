import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import React, {Component} from 'react';
import {StyleSheet,Text, View,Image,TouchableOpacity} from 'react-native';

class MultiSelectComp extends Component {
  
    onSelectedItemsChange(selectedItems) {
      this.props.onChange(selectedItems);
    };

    render() {
        return (
          <View style={styles.basic}>
            <Text style={{fontSize: 18, paddingLeft: 10, paddingTop: 12, marginTop:5, marginRight:-20, color: this.props.textColor}} >{this.props.label + ":"}</Text>
            <SectionedMultiSelect
              items={this.props.items}
              uniqueKey="id"
              selectText=""
              showDropDowns={true}
              onSelectedItemsChange={(value) => this.onSelectedItemsChange(value)}
              selectedItems={this.props.selectedItems}
              selectedText="נבחרו"
              confirmText="אישור"
              searchPlaceholderText="חפש קטגוריות"
              styles={{chipsWrapper :{flexDirection: 'row-reverse',paddingRight:"20%"},
                       chipContainer:{borderWidth:0,borderRadius:0,borderBottomWidth:0,borderBottomColor:"#b76c94"},
                       chipText:{color: 'black',fontSize: 18,fontWeight: 'bold'}}}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    basic:{
      flexDirection: 'row-reverse',
      width: '90%'
    },
    firstLabelStyle: {
      fontSize: 18,
      paddingLeft: 10,
      paddingTop: 12,
      marginTop:5,
      marginRight:-20
    }
  });

export {MultiSelectComp};