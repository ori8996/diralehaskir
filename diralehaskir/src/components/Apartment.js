import React, {Component} from 'react';
import {Text, View,Image } from 'react-native';
import { Card } from 'react-native-elements';
import { Icon } from 'react-native-elements';
import firebase from 'firebase';
import constant from '../const';
import theme from '../themes/mainTheme';
import AwesomeButton from "react-native-really-awesome-button";

class Apartment extends Component {
  state = {Image: require('../../images/placeholder.jpg'), loading: false, neighborhood: ""};

  componentDidMount(){
    const storageRef = firebase.storage().ref();
    if(this.props.apartment.data.apartment_data.photos && this.props.apartment.data.apartment_data.photos.length != 0){
      this.setState({loading: true});
      storageRef.child(this.props.apartment.id + "/" + this.props.apartment.data.apartment_data.photos[0]).getDownloadURL().then((url) => {
        this.setState({Image: {uri: url}, loading: false})
      }).catch(function(error) {
        console.log(imagePath, ": error ", error);
      });
    }
    
    for(let neighborhood of this.props.neighborhoods){
      if(neighborhood.ref.isEqual(this.props.apartment.data.apartment_data.neighborhood)){
        this.setState({neighborhood: neighborhood.value});
      }
    }
  }

  render() {
    if (this.state.loading) {
      return (<Text> </Text>);
    }
    return (
      <View style={{flexDirection:"row",justifyContent:"center"}}>
        <Card containerStyle={{width:"85%",height:320,borderWidth:4,borderColor:"#4d263c",borderRadius:10}}>
          <Image 
          source={ this.state.Image}
          style={{marginLeft:0,marginBottom: 10, width: "100%", height: "85%"}}
          /> 
          <View style={{flex: 1, flexDirection: 'row',justifyContent:"flex-end"}}>
            <View>
              <AwesomeButton style={{marginTop: 10, alignSelf: 'center'}} 
                              backgroundColor={"#732F72"} textColor={"white"} height={40} width={80}
                              borderRadius={15} textSize={16}
                              onPress={()=> this.props.onPress(this.props.apartment.id, this.props.apartment.myApartmentId, this.state.neighborhood)}>לפרטים</AwesomeButton>
              {this.props.isAdmin ? 
              <Text style={theme[constant.statusDisplay[this.props.apartment.data.status].class]}>         
                {constant.statusDisplay[this.props.apartment.data.status].display}
              </Text> : <Text>  </Text>}
            </View>
            <View style={{marginTop:-5,marginRight:"2%",width:"70%"}}>
            <Text style={{fontSize:14}}>
                סוג נכס: {this.props.apartment.data.apartment_data.homeType}
              </Text>
              <Text style={{fontSize:14}}>
                שכונה {this.state.neighborhood}
              </Text>
              <Text style={{fontSize:14}}>
                עולה {this.props.apartment.data.apartment_data.price} ש"ח
              </Text>
              <Text style={{fontSize:14}}>
                בעלת {this.props.apartment.data.apartment_data.rooms} חדרים בקומה {this.props.apartment.data.apartment_data.floor}
              </Text>
            </View>
          </View>
        </Card>
      </View>
    );
  }
}

export default Apartment;

