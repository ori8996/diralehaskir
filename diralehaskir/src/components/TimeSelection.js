import React, {Component} from 'react';
import { View,ScrollView } from 'react-native';
import { Button, Text } from 'react-native-elements';
import firebase from 'firebase';
import DateTimePicker from '@react-native-community/datetimepicker';
import { AirbnbRating } from 'react-native-ratings';
import {withNavigation} from 'react-navigation';
import { SCLAlert, SCLAlertButton } from 'react-native-scl-alert';
import { Icon } from 'react-native-elements';
 
Date.prototype.addMonths = function (m) {
  var d = new Date(this);
  var years = Math.floor(m / 12);
  var months = m - (years * 12);
  if (years) d.setFullYear(d.getFullYear() + years);
  if (months) d.setMonth(d.getMonth() + months);
  return d;
} 

class TimeSelection extends Component {
  state = {show: false, date: new Date().addMonths(2), partition: 5, state: 5, owner: 5, location: 5, items: 5, visible: false};
  
  onChange = (event, selectedDate) => {
    if (event.type === "dismissed"){
        this.setState({show: false});
    } else {
        const currentDate = selectedDate || this.state.date;
        this.setState({show: false, date: currentDate});
    }
  };

  ratingCompleted(rating, field) {
    this.setState({[field]: rating});
  }

  save(){
    const saveToDB = {"apartment_rating" : {partition: this.state.partition, state: this.state.state, owner: this.state.owner, location: this.state.location, items: this.state.items}, "timeSelection": this.state.date.getTime() };
    var userId = firebase.auth().currentUser.uid;
    const db = firebase.firestore();
    db.collection('users').doc(userId).update(saveToDB);
    this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "GenerateApartments" }});
  }

  render() {
    return (
        <ScrollView style={{marginRight:10}}>
          <View style={{flexDirection: 'row'}}>
            <Icon containerStyle={{ marginTop:"9%", marginLeft:"11%"}} size={28} name='help-circle' type='feather' color="#334693" onPress={()=>{this.setState({visible: true})}}/>
            <Text  style={{marginTop:10, marginLeft:10, fontSize: 20,flex: 1}} >
              כחלק מתהליך חיפוש הדירות המתאימות, חשוב לנו לדעת כמה זמן יש לך לחיפושים
            </Text>
          </View>
          <View>
            <Button containerStyle={{padding:10,width:"60%"}} onPress={()=>{this.setState({show: true})}} title="לחץ לבחירת תאריך יעד" />
          </View>
          {this.state.show && (
            <DateTimePicker
              testID="dateTimePicker"
              value={this.state.date}
              mode='date'
              is24Hour={true}
              display="default"
              onChange={this.onChange}
            />
          )} 
          {this.state.date && (<Text style={{marginTop:10, marginLeft:10, fontSize: 16,fontWeight:"bold"}} >
            התאריך הנבחר הוא {this.state.date.getDate()}/{this.state.date.getMonth()+1}/{this.state.date.getFullYear()}
          </Text>)}
          <Text  style={{marginTop:20, marginLeft:10, fontSize: 20}} >
            בנוסף, דרג את התכונות הבאות כדי שנדע כמה כל תכונה חשובה לך:
          </Text>

          <View style={{marginTop:15, marginLeft:10}}>
                <Text style={{fontSize:18}}>גודל וחלוקת הדירה: </Text>
                <AirbnbRating style={{marginTop:2, marginLeft:10}}
                count={10}
                reviews={[]}
                defaultRating={this.state.partition}
                size={24}
                reviewSize={0}
                onFinishRating={(rating) => (this.ratingCompleted(rating, "partition"))}
                />
                <Text style={{marginTop:15,fontSize:18}}>מצב הדירה: </Text>
                <AirbnbRating
                count={10}
                reviews={[]}
                defaultRating={this.state.state}
                size={24}
                reviewSize={0}
                onFinishRating={(rating) => (this.ratingCompleted(rating, "state"))}
                />
                <Text style={{marginTop:15,fontSize:18}}>בעל הדירה: </Text>
                <AirbnbRating
                count={10}
                reviews={[]}
                defaultRating={this.state.owner}
                size={24}
                reviewSize={0}
                onFinishRating={(rating) => (this.ratingCompleted(rating, "owner"))}
                />
                <Text style={{marginTop:15,fontSize:18}}>סביבת הדירה: </Text>
                <AirbnbRating
                count={10}
                reviews={[]}
                defaultRating={this.state.location}
                size={24}
                reviewSize={0}
                onFinishRating={(rating) => (this.ratingCompleted(rating, "location"))}
                />
                <Text style={{marginTop:15,fontSize:18}}>אבזור הדירה: </Text>
                <AirbnbRating
                count={10}
                reviews={[]}
                defaultRating={this.state.items}
                size={24}
                reviewSize={0}
                onFinishRating={(rating) => (this.ratingCompleted(rating, "items"))}
                />
                <Button containerStyle={{padding:10}} title='הבא' onPress={this.save.bind(this)}/>

                <SCLAlert
                  theme="info"
                  show={this.state.visible}
                  title="מדוע עליי למלא תאריך?">
                    <Text style={{fontSize:20,marginTop:-40}}>הסיבה היא כדי שנוכל לחשב בכל זמן נתון כמה זמן נותר לך לחיפוש, וכך לייעץ לך ברגע נתון האם לקחת את הדירה או להמשיך את החיפושים.</Text>
                  <SCLAlertButton theme="info" onPress={() => {this.setState({visible:false});}}>
                      חזור
                  </SCLAlertButton>
                </SCLAlert>
            </View>
        </ScrollView>
      );
  }
}

export default withNavigation(TimeSelection);