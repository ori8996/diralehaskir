import React, {Component} from 'react';
import {Image, ImageBackground, Dimensions, BackHandler } from 'react-native';
import {withNavigation} from 'react-navigation';
import { Header,ThemeProvider } from 'react-native-elements';
import firebase from 'firebase';
import Apartments from './Apartments';
import RatedApartments from './RatedApartments';
import AddApartment from './AddApartment';
import BasicQuestions from './BasicQuestions';
import GenerateApartments from './GenerateApartments';
import TimeSelection from './TimeSelection';
import ExplanationPage from './ExplanationPage';
import theme from '../themes/mainTheme';

class Main extends Component {
  state={isProfile:2, isAdmin: false, neighborhoods:[]};

  signOut(){
    firebase.auth().signOut();
    this.props.navigation.navigate('master', { screen: 'Home' });
  }

  componentDidMount() {
      this.userId = firebase.auth().currentUser.uid;
      firebase.firestore().collection("users").doc(this.userId).get().then((querySnapshot) => {
        let isAdmin = querySnapshot.data().isAdmin ? true : false;
        this.setState({isProfile: Number(Object.keys(querySnapshot.data().apartment_prefernces).length != 0), isAdmin: isAdmin});
      });
      
      var that = this;

      if(this.state.neighborhoods.length == 0){
        firebase.firestore().collection("neighborhoods").get().then(function(querySnapshot) {
          querySnapshot.forEach(doc => {
            const obj = {'key':doc.id, 'value':doc.data().name,'ref':doc.ref};
            that.setState({ neighborhoods: [...that.state.neighborhoods, obj]});
          });
        });
      }
      
      BackHandler.addEventListener('hardwareBackPress', () => {return true});
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', () => {return true});
  }


  renderContent() {

    if(this.state.isAdmin && !this.props.route.params){
      return (<Apartments navigation={this.props.navigation} isAdmin={this.state.isAdmin} neighborhoods={this.state.neighborhoods}></Apartments>);
    }
    
    if((this.state.isProfile === 1 && !this.props.route.params) || 
      (this.props.route.params && 
      this.props.route.params.screen == "apartments")){
      return (<Apartments navigation={this.props.navigation} isAdmin={this.state.isAdmin} neighborhoods={this.state.neighborhoods}></Apartments>);
    }

    if((!this.state.isProfile === 0 && !this.props.route.params) ||
      (this.props.route.params && 
      this.props.route.params.screen == "basicQuestions")){
      return (<BasicQuestions navigation={this.props.navigation} neighborhoods={this.state.neighborhoods}></BasicQuestions>);
    }

    if(this.props.route.params && 
      this.props.route.params.screen == "addApartment"){
       return (<AddApartment navigation={this.props.navigation} neighborhoods={this.state.neighborhoods}></AddApartment>);
    }

    if(this.props.route.params && 
      this.props.route.params.screen == "timeSelection"){
       return (<TimeSelection navigation={this.props.navigation}></TimeSelection>);
    }

    if(this.props.route.params && 
      this.props.route.params.screen == "explanationPage"){
       return (<ExplanationPage navigation={this.props.navigation}></ExplanationPage>);
    }

    if(this.props.route.params && 
      this.props.route.params.screen == "GenerateApartments"){
      return (<GenerateApartments navigation={this.props.navigation}></GenerateApartments>);
    }

    if(this.props.route.params && 
      this.props.route.params.screen == "RatedApartments"){
      return (<RatedApartments navigation={this.props.navigation} isAdmin={this.state.isAdmin} neighborhoods={this.state.neighborhoods}></RatedApartments>);
    }
  }

  render () {
    const { height } = Dimensions.get('window');
    return (
      <ImageBackground
      source={require('../../images/back.jpg')}
      style={{width: '100%', height: '100%',position:"absolute"}}>
        <ThemeProvider theme={theme}>
          <Header backgroundColor="transperent" containerStyle={{height: '15%',marginTop:-15}}
            leftComponent={{ icon: 'logout', type:'simple-line-icon', color: 'black',onPress:() => this.signOut() }}
            centerComponent={<Image source={require('../../images/diralehaskir.png')} style={{ width: 140, height: 100 }}></Image>}
            rightComponent={{ icon: 'menu', color: 'black',onPress:() => this.props.navigation.toggleDrawer() }}
          />
          {this.renderContent()}
        </ThemeProvider>
      </ImageBackground>
    );
  }   
}

export default withNavigation(Main);
