import React, {Component} from 'react';
import {Text, ScrollView, View } from 'react-native';
import {AutoComplete, MultiSelectComp, Spinner} from './common';
import {withNavigation} from 'react-navigation';
import EStyleSheet from 'react-native-extended-stylesheet';
import firebase from 'firebase';
import {Button, ThemeProvider, Header, Input, CheckBox } from 'react-native-elements';
import theme from '../themes/mainTheme';
import { Hoshi } from 'react-native-textinput-effects';
import RNPickerSelect from 'react-native-picker-select';


class BasicQuestions extends Component {
  homeTypes = [{
    id: '1',
    name: 'דירה'
  }, {
    id: '2',
    name: 'דירת גן'
  }, {
    id: '3',
    name: 'דופלקס'
  }, {
    id: '4',
    name: 'קוטג\''
  }, {
    id: '5',
    name: 'פנטהאוז'
  }
];

  data = [{ label: '1', value: '1' },{ label: '1.5', value: '1.5' },{ label: '2', value: '2' },
            { label: '2.5', value: '2.5' },{ label: '3', value: '3' },{ label: '3.5', value: '3.5' },
            { label: '4', value: '4' },{ label: '4.5', value: '4.5' },{ label: '5', value: '5' },
            { label: '6', value: '6' },{ label: '7', value: '7' },{ label: '8', value: '8' },
            { label: '9', value: '9' },{ label: '10', value: '10' },{ label: '11', value: '11' },
            { label: '12', value: '12' },{ label: '13', value: '13' },{ label: '14', value: '14' }];
            
  state = {apartment_prefernces: {price:0, ac: false, accessible: false, balcony: false, elevator: false,
          bars: false, furnished: false, parking: false, partners: false, pets: false,
          rooms: 0, shelter: false, size: 0, storeroom: false, neighborhood:'', homeType:[], longTerm: false},
          isNew: true,size:"",rooms:"",neighborhoods:[], loading: false, neighborhoodName:"", roomColor: 'black', homeColor: 'black',
          sizeColor: 'black',neighborhoodColor: 'black', isValid: true};

  componentDidMount() {
    this.init();
  }

  init(){
    this.setState({loading: true});
    var that = this;
    const db = firebase.firestore();
    var userId = firebase.auth().currentUser.uid;

    db.collection("neighborhoods").get().then(function(querySnapshot) {
      querySnapshot.forEach(doc => {
        const obj = {'key':doc.id, 'value':doc.data().name,'ref':doc.ref};
        that.setState({ neighborhoods: [...that.state.neighborhoods, obj],load:false});
      });
    });

    db.collection("users").doc(userId).get().then((querySnapshot) => {
      if(Object.keys(querySnapshot.data().apartment_prefernces).length != 0){
        this.setState({apartment_prefernces: querySnapshot.data().apartment_prefernces,
                       size: querySnapshot.data().apartment_prefernces.size.toString(),
                       rooms: querySnapshot.data().apartment_prefernces.rooms.toString(),
                       isNew: false});
        for(let neighborhood of this.state.neighborhoods){
          if(neighborhood.ref.isEqual(querySnapshot.data().apartment_prefernces.neighborhood)){
            this.setState({neighborhoodName: neighborhood.value});
          }
        }
      }

      this.setState({loading: false});
    });
  }

  onButtonPress() {
    if(this.state.apartment_prefernces.rooms === 0 || this.state.apartment_prefernces.size === 0 || 
       this.state.apartment_prefernces.homeType.length === 0 || !this.state.apartment_prefernces.neighborhood) {
         this.setState({isValid: false})
         if(this.state.apartment_prefernces.rooms == 0) {
          this.setState({roomColor: "red"});
         }
         if(this.state.apartment_prefernces.size == 0) {
          this.setState({sizeColor: "red"});
         }
         if(this.state.apartment_prefernces.homeType.length == 0) {
          this.setState({homeColor: "red"});
         }
         if(!this.state.apartment_prefernces.neighborhood) {
          this.setState({neighborhoodColor: "red"});
         }
         this.refs._scrollView.scrollTo(0);
         return;
       }
    const dbData = {apartment_prefernces : this.state.apartment_prefernces };
    var userId = firebase.auth().currentUser.uid;
    const db = firebase.firestore();
    
    this.generatePrice(db).then((price) => {
      dbData.apartment_prefernces.price = price;
      if(!this.state.isNew){
        var storageRef = firebase.storage().ref();

        var desertRef = storageRef.child(userId + "/model.pkl");
        // Delete the file
        if(desertRef){
          desertRef.delete().then(function() {
            console.log("Model deleted successfully");
          }).catch(function(error) {
            console.log("error " + JSON.stringify(error));
          });
        }

        var batch = firebase.firestore().batch()

        db.collection('users').doc(userId).collection("my_apartments").get().then((querySnapshot) => {
          querySnapshot.forEach(doc => {
            console.log(doc.ref)
            batch.delete(doc.ref);
          });

          db.collection('users').doc(userId).collection("my_rated_apartments").get().then((querySnapshot) => {
            querySnapshot.forEach(doc => {
              console.log(doc.ref)
              batch.delete(doc.ref);
            });

            batch.commit();
          }).catch(function(error) {
              console.error("Error removing my_rated_apartments ", error);
          });
        }).catch(function(error) {
            console.error("Error removing my_apartments ", error);
        });
      }

      db.collection('users').doc(userId).update(dbData);
      this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "timeSelection" }});
    });
 }

  generatePrice(db) {
    let currData = this.state.apartment_prefernces;
    let currSize = parseInt(currData.size);
    let avgRooms = parseInt(currData.rooms);
    let firstAprt;
    let secAprt;
    let isInTheMiddle = false;
    let sameNeighborhoodQuery = db.collection('apartments').
                                  where('apartment_data.neighborhood', '==', currData.neighborhood).
                                  where('status', 'in', [2, 4]);
    let relevantApartments = [];
    return sameNeighborhoodQuery.get().then(snapshot => {
    if (snapshot.empty) {
      console.log('No matching documents.');
      return 0;
    }
    snapshot.forEach(doc => {
      relevantApartments.push(doc.data());
    });
    
    relevantApartments = relevantApartments.sort((a, b) => a.apartment_data.size - b.apartment_data.size);
    for (let index = 0; index < relevantApartments.length; index++) {
      const element = relevantApartments[index].apartment_data;
      if(element.size > currSize) {
        if(index == 0) {
          firstAprt = relevantApartments[index].apartment_data;
          if(relevantApartments.length > 1)
          {
            secAprt = relevantApartments[relevantApartments.length - 1].apartment_data;
          }
        }
        else {
          firstAprt = relevantApartments[index - 1].apartment_data;
          secAprt = relevantApartments[index].apartment_data;
          isInTheMiddle = true;
        }
        break;
      }
    }
    if(!firstAprt) {
      if(relevantApartments.length > 1) {
        firstAprt = relevantApartments[0].apartment_data;
        secAprt = relevantApartments[relevantApartments.length - 1].apartment_data;
      } else if (relevantApartments.length == 1) {
        firstAprt = relevantApartments[0].apartment_data;
      }
    }
    let newMoney;
    if(firstAprt) {
      if(isInTheMiddle) {
        let dist = secAprt.size - firstAprt.size;
        let currDist = currSize - firstAprt.size;
        let closePercent = currDist / dist;
        if(firstAprt.price > secAprt.price) {
          closePercent = 1 - closePercent;
        }
        let moneyToAdd = parseInt(Math.abs(secAprt.price - firstAprt.price) * closePercent, 10);
        newMoney = Math.min(secAprt.price, firstAprt.price) + moneyToAdd;
        if(avgRooms > firstAprt.rooms && avgRooms > secAprt.rooms) {
          newMoney += parseInt((avgRooms - Math.max(firstAprt.rooms, secAprt.rooms)) * 200, 10);
        } else if(avgRooms < firstAprt.rooms && avgRooms < secAprt.rooms) {
          newMoney -= parseInt((Math.min(firstAprt.rooms, secAprt.rooms) - avgRooms) * 200, 10);
        }
        if(currData.balcony && !firstAprt.balcony && !secAprt.balcony) {
          newMoney += 200;
        }
        else if(!currData.balcony && firstAprt.balcony && secAprt.balcony) {
          newMoney -= 200;
        }
        if(currData.parking && !firstAprt.parking && !secAprt.parking) {
          newMoney += 150;
        }
        else if(!currData.parking && firstAprt.parking && secAprt.parking) {
          newMoney -= 150;
        }
        if(currData.storeroom && !firstAprt.storeroom && !secAprt.storeroom) {
          newMoney += 150;
        }
        else if(!currData.storeroom && firstAprt.storeroom && secAprt.storeroom) {
          newMoney -= 150;
        }
        if(currData.shelter && !firstAprt.shelter && !secAprt.shelter) {
          newMoney += 100;
        }
        else if(!currData.shelter && firstAprt.shelter && secAprt.shelter) {
          newMoney -= 100;
        }
        if(currData.refurbished && !firstAprt.refurbished && !secAprt.refurbished) {
          newMoney += 150;
        }
        else if(!currData.refurbished && firstAprt.refurbished && secAprt.refurbished) {
          newMoney -= 150;
        }
        if(currData.furnished && !firstAprt.furnished && !secAprt.furnished) {
          newMoney += 100;
        }
        else if(!currData.furnished && firstAprt.furnished && secAprt.furnished) {
          newMoney -= 100;
        }
      }
      else {
        if(secAprt) {
          let sizeDist = secAprt.size - firstAprt.size;
          let priceDist = secAprt.price - firstAprt.price;
          if(sizeDist != 0) {
            let relativePrice = priceDist / sizeDist;
            let moneyToAdd = parseInt((currSize - firstAprt.size) * relativePrice, 10);
            newMoney =  Math.min(secAprt.price, firstAprt.price) + moneyToAdd;
          }
          else {
            newMoney = parseInt((secAprt.price + firstAprt.price) / 2, 10);
          }
          let closestAprt;
          if(currSize > secAprt.size){
            closestAprt = secAprt;
          } else {
            closestAprt = firstAprt;
          }
          newMoney += parseInt((avgRooms - closestAprt.rooms) * 100, 10);
          
          if(currData.balcony && !closestAprt.balcony) {
            newMoney += 100;
          }
          else if(!currData.balcony && closestAprt.balcony) {
            newMoney -= 100;
          }
          if(currData.parking && !closestAprt.parking) {
            newMoney += 75;
          }
          else if(!currData.parking && closestAprt.parking) {
            newMoney -= 75;
          }
          if(currData.storeroom && !closestAprt.storeroom) {
            newMoney += 75;
          }
          else if(!currData.storeroom && closestAprt.storeroom) {
            newMoney -= 75;
          }
          if(currData.shelter && !closestAprt.shelter) {
            newMoney += 50;
          }
          else if(!currData.shelter && closestAprt.shelter) {
            newMoney -= 50;
          }
          if(currData.refurbished && !closestAprt.refurbished) {
            newMoney += 75;
          }
          else if(!currData.refurbished && closestAprt.refurbished) {
            newMoney -= 75;
          }
          if(currData.furnished && !closestAprt.furnished) {
            newMoney += 50;
          }
          else if(!currData.furnished && closestAprt.furnished) {
            newMoney -= 50;
          }
        }
        else {
          newMoney = firstAprt.price;
        }
      }
    }
    let numOfTypes = 0;
    let dira = 0, gan = 0, kotej = 0, duplex = 0, penthouse = 0;
    if(currData.homeType.includes("1")) {
      numOfTypes++;
      dira = newMoney;
    }
    if(currData.homeType.includes("2")) {
      numOfTypes++;
      gan = newMoney * 1.05;
    }
    if(currData.homeType.includes("4")) {
      numOfTypes++;
      kotej = newMoney * 1.15;
    }
    if(currData.homeType.includes("3")) {
      numOfTypes++;
      duplex = newMoney * 1.1;
    }
    if(currData.homeType.includes("5")) {
      numOfTypes++;
      penthouse = newMoney * 1.22;
    }
    if(numOfTypes > 0) {
      newMoney = parseInt((dira + gan + kotej + duplex + penthouse) / numOfTypes, 10);
    }
    
    return newMoney - (newMoney % 50);
  })
  .catch(err => {
    console.log('Error getting documents', err);
  });

  }

  render () {
    if (this.state.loading) {
      return (<Spinner size="large"/>);
    }
    return (
        <ScrollView ref='_scrollView' style={{marginTop:5}}>
        <Text style={theme.title} > מילוי פרטים אישיים</Text>
        {!this.state.isValid && <Text style={{fontSize: 15, alignSelf: 'center', color: 'red'}} hide={true} >אנא מלא את הפרטים החסרים</Text>}
        <View style={{marginBottom:"5%"}}>
          <MultiSelectComp
          textColor = {this.state.homeColor}
          selectedItems={this.state.apartment_prefernces.homeType}
          label = "סוג נכס"
          onChange={homeType => {this.setState({apartment_prefernces: {...this.state.apartment_prefernces, homeType: homeType},
                                                homeColor: "black"})}}
          items={this.homeTypes}/>
        </View>
        <View style={styles.containerStyle}>
            <Text style={{marginRight:20,fontSize:18,marginTop:"9%",color:this.state.roomColor,marginLeft:5}}>מספר חדרים:</Text>
            <RNPickerSelect
            onValueChange={rooms => {this.setState({apartment_prefernces: {...this.state.apartment_prefernces, rooms: Number(rooms)},rooms:rooms,
                                                    roomColor: "black"})}}
            value = {this.state.rooms}
            items={this.data}
            placeholder={{label:"בחר",value:null}}
            style={{inputAndroid:{color:"black",marginLeft:"8%"},
                    placeholder:{color: 'black',fontWeight:"bold",fontSize:18},
                    viewContainer:{width:"62%",marginTop:15,marginRight:"4%",borderBottomColor:"black",borderBottomWidth:2}}}/>
        </View>
        <View style={styles.containerStyle}>
          <Text style={{fontSize:18,marginTop:"4%",color:this.state.sizeColor,marginLeft:5,marginRight:"5%"}}>גודל:</Text>
          <Hoshi
            style={{width:"62%",marginRight:"19%",marginTop:-20,borderBottomColor:"black",borderBottomWidth:2.5}}
            inputStyle={{fontWeight:"normal",fontSize:16,marginTop:50,textAlign :"left",marginRight:"35%",color:"black"}}
            borderHeight={0}
            value={this.state.size}
            onChangeText={size => {
                if(!isNaN(size)) {
                  this.setState({apartment_prefernces: {...this.state.apartment_prefernces, size: Number(size)},size:size,
                  sizeColor: "black"})
                }
              }
            }
            keyboardType="numeric"
            maxLength={3}
          />
        </View>
        <View style={styles.containerStyle}>
            <Text style={{marginRight:22,width:'15%',fontSize:18,marginTop:15,color:this.state.addressColor}}>עיר:</Text>
            <Text style={{width:"78%",fontSize:18,marginTop:15,fontWeight:"bold"}}>תל אביב</Text>
        </View>
        <View style={theme.detailsView}>
          <AutoComplete
          textColor = {this.state.neighborhoodColor}
          value= {this.state.neighborhoodName}
          label="שכונה מועדפת"
          data={this.state.neighborhoods} 
          onChange={neighborhood => {this.setState({apartment_prefernces: {...this.state.apartment_prefernces, neighborhood: neighborhood.ref},
                                                         neighborhoodName: neighborhood.value,
                                                         neighborhoodColor: "black"});}} />
        </View>
        <Text style={{marginTop:"7%",marginBottom:"3%", marginRight:5,fontSize: 25,color:"#4d263c",fontWeight:"bold"}} > סמן את הפרטים החשובים לך:</Text>
        <View style={theme.detailsView}>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="מזגן"
            checked={this.state.apartment_prefernces.ac}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, ac: !this.state.apartment_prefernces.ac}})}/>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="נגישות"
            checked={this.state.apartment_prefernces.accessible}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, accessible: !this.state.apartment_prefernces.accessible}})}/>
        </View>
        <View style={theme.detailsView}>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="מרפסת"
            checked={this.state.apartment_prefernces.balcony}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, balcony: !this.state.apartment_prefernces.balcony}})}/>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="מעלית"
            checked={this.state.apartment_prefernces.elevator}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, elevator: !this.state.apartment_prefernces.elevator}})}/>
        </View>
        <View style={theme.detailsView}>
        <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="סורגים"
            checked={this.state.apartment_prefernces.bars}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, bars: !this.state.apartment_prefernces.bars}})}/>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="מרוהטת"
            checked={this.state.apartment_prefernces.furnished}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, furnished: !this.state.apartment_prefernces.furnished}})}/>
        </View>
        <View style={theme.detailsView}>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="חנייה"
            checked={this.state.apartment_prefernces.parking}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, parking: !this.state.apartment_prefernces.parking}})}/>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="שותפים"
            checked={this.state.apartment_prefernces.partners}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, partners: !this.state.apartment_prefernces.partners}})}/>
        </View>
        <View style={theme.detailsView}>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="בעלי חיים"
            checked={this.state.apartment_prefernces.pets}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, pets: !this.state.apartment_prefernces.pets}})}/>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="מקלט"
            checked={this.state.apartment_prefernces.shelter}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, shelter: !this.state.apartment_prefernces.shelter}})}/>
        </View>  
        <View style={theme.detailsView}>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="מחסן"
            checked={this.state.apartment_prefernces.storeroom}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, storeroom: !this.state.apartment_prefernces.storeroom}})}/>
          <CheckBox
            iconRight
            right
            containerStyle={theme.checkBoxStyle}
            textStyle={{color:"black",fontSize:16}}
            uncheckedColor={"black"}
            checkedColor={"#732F72"}
            title="לטווח ארוך"
            checked={this.state.apartment_prefernces.longTerm}
            onPress={() => this.setState({apartment_prefernces: {...this.state.apartment_prefernces, longTerm: !this.state.apartment_prefernces.longTerm}})}/>
        </View>
        <Button containerStyle={{marginTop:10,padding:10}} title='המשך' onPress={this.onButtonPress.bind(this)}/>
      </ScrollView>
    );
  }   
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
  },
  inner: {
    justifyContent: "flex-end"
  },
  containerStyle: {
    flexDirection: 'row-reverse',
    marginTop: 5,
    marginBottom: 20
  },
});



export default withNavigation(BasicQuestions);
