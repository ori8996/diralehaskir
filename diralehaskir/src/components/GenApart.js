import React, {
  Component,
} from 'react';

import {
  LayoutAnimation,
  UIManager,
  View,
} from 'react-native';

import FoldView from 'react-native-foldview';

import UpperCard from './FoldViewComp/UpperCard';
import LowerCard from './FoldViewComp/LowerCard';

// Enable LayoutAnimation on Android
if (UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

const GenApart_HEIGHT = 220;

const Spacer = ({ height }) => (
  <View
    pointerEvents="none"
    style={{
      height,
    }}
  />
);

export default class GenApart extends Component {

  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      height: GenApart_HEIGHT,
    };
  }

  componentWillMount() {
    this.flip = this.flip.bind(this);
    this.handleAnimationStart = this.handleAnimationStart.bind(this);
    this.renderFrontface = this.renderFrontface.bind(this);
    this.renderBackface = this.renderBackface.bind(this);
  }

  flip() {
    this.setState({
      expanded: !this.state.expanded,
    });
  }

  handleAnimationStart(duration, height) {
    // const isExpanding = this.state.expanded;

    // const animationConfig = {
    //   duration,
    //   update: {
    //     type: isExpanding ? LayoutAnimation.Types.easeOut : LayoutAnimation.Types.easeIn,
    //     property: LayoutAnimation.Properties.height,
    //   },
    // };

    // LayoutAnimation.configureNext(animationConfig);

    this.setState({
      height,
    });
  }

  renderFrontface() {
    return (
      <UpperCard updateRate={this.props.updateRate} flip={this.state.expanded} apart={this.props.apartment} onPress={this.flip} />
    );
  }

  renderBackface() {
    return (
      <LowerCard flip={this.state.expanded} apart={this.props.apartment} onPress={this.flip} />
    );
  }

  render() {
    const { height } = this.state;
    //const { zIndex } = this.props;

    const spacerHeight = height - GenApart_HEIGHT;

    return (
      <View
        style={{
          flex: 1
        }}
      >
        <View
          style={{
            height: GenApart_HEIGHT,
            margin: 10,
          }}
        >
          <FoldView
            expanded={this.state.expanded}
            onAnimationStart={this.handleAnimationStart}
            perspective={1000}
            renderBackface={this.renderBackface}
            renderFrontface={this.renderFrontface}
          >
            <UpperCard flip={this.state.expanded} apart={this.props.apartment} onPress={this.flip} />
          </FoldView>

        </View>

        <Spacer height={spacerHeight} />
      </View>
    );
  }
}
