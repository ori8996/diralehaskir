import React, {Component} from 'react';
import {Text, ScrollView, View, Picker } from 'react-native';
import {AutoComplete,Spinner} from './common';
import {withNavigation} from 'react-navigation';
import EStyleSheet from 'react-native-extended-stylesheet';
import firebase from 'firebase';
import {Button, CheckBox } from 'react-native-elements';
import theme from '../themes/mainTheme';
import ImagePicker from 'react-native-image-picker';
import { SCLAlert, SCLAlertButton } from 'react-native-scl-alert';
import { Hoshi } from 'react-native-textinput-effects';
import RNPickerSelect from 'react-native-picker-select';

class AddApartment extends Component {
  homeTypes = ['דירה','דירת גן','דופלקס','קוטג','פנטהאוז'];
  data = [{ label: '1', value: '1' },{ label: '1.5', value: '1.5' },{ label: '2', value: '2' },
          { label: '2.5', value: '2.5' },{ label: '3', value: '3' },{ label: '3.5', value: '3.5' },
          { label: '4', value: '4' },{ label: '4.5', value: '4.5' },{ label: '5', value: '5' },
          { label: '6', value: '6' },{ label: '7', value: '7' },{ label: '8', value: '8' },
          { label: '9', value: '9' },{ label: '10', value: '10' },{ label: '11', value: '11' },
          { label: '12', value: '12' },{ label: '13', value: '13' },{ label: '14', value: '14' }];
  state = {userData: {apartment_data: {ac: false, accessible: false, balcony: false, elevator: false, equipped_kitchen: false, floor: 0, refurbished: false, bars: false,
          furnished: false, parking: false, partners: false, pets: false, price: 0, rooms:0, shelter: false, 
          size: 0, storeroom: false, neighborhood:'', address:'', homeType:"דירה", longTerm: false,photos:[]},
          contact: {name: '', phone: 0}, status:1 },size:"",rooms:"",price:"", floor: "",
          photos:[], neighborhoods:[],visible: false,loading:false, roomsColor: 'black', phoneColor: 'black',
          sizeColor: 'black',neighborhoodColor: 'black', addressColor: 'black', nameColor: 'black', priceColor: 'black', isValid: true, load:false};

  componentDidMount() {
    this.setState({load:true})
    var that = this;
    const db = firebase.firestore();
    db.collection("neighborhoods").get().then(function(querySnapshot) {
      querySnapshot.forEach(doc => {
        const obj = {'key':doc.id, 'value':doc.data().name,'ref':doc.ref};
        that.setState({ neighborhoods: [...that.state.neighborhoods, obj],load:false});
      });
    });
  }

  Save() {
    if(this.state.userData.contact.name == '' || this.state.userData.contact.phone == 0 || this.state.userData.apartment_data.address == '' || 
    !this.state.userData.apartment_data.neighborhood || this.state.userData.apartment_data.neighborhood == '' || this.state.userData.apartment_data.price == 0 || 
    !this.state.userData.apartment_data.rooms || this.state.userData.apartment_data.size == 0) {
      this.setState({isValid: false});
      if(this.state.userData.contact.name == '') {
        this.setState({nameColor: "red"});
       }
       if(this.state.userData.contact.phone == 0) {
        this.setState({phoneColor: "red"});
       }
       if(this.state.userData.apartment_data.address == '') {
        this.setState({addressColor: "red"});
       }
       if(!this.state.userData.apartment_data.neighborhood || this.state.userData.apartment_data.neighborhood == '') {
        this.setState({neighborhoodColor: "red"});
       }
       if(!this.state.userData.apartment_data.rooms) {
        this.setState({roomsColor: "red"});
       }
       if(this.state.userData.apartment_data.size == 0) {
        this.setState({sizeColor: "red"});
       }
       if(this.state.userData.apartment_data.price == 0) {
        this.setState({priceColor: "red"});
       }
       this.refs._scrollView.scrollTo(0);
       return;
    }

    this.setState({loading:true});
    let apartToSave = this.state.userData;
    apartToSave.apartment_data.neighborhood = firebase.firestore().collection('neighborhoods').doc(apartToSave.apartment_data.neighborhood);
      firebase.firestore().collection("apartments").add({...apartToSave, apartment_data: {...apartToSave.apartment_data, 
        photos: this.state.photos.map(photo => photo.fileName)}}).then((docRef) => {
          this.state.photos.forEach(photo => {
              this.uriToBlob(photo.uri).then((blob)=> {
                  this.uploadToFirebase(blob, docRef.id + "/" + photo.fileName).then((snapshot)=>{
                      console.log("File uploaded");
                  }).catch((error)=>{
                      console.error("Error saving photo: ", error);
                  })
              });
          });
          this.setState({userData: {apartment_data: {ac: false, accessible: false, balcony: false, elevator: false, equipped_kitchen: false, floor: 0, refurbished: false, bars: false,
                      furnished: false, parking: false, partners: false, pets: false, price: 0, rooms: 0, shelter: false, 
                      size: 0, storeroom: false, neighborhood:'', address:'', homeType:"דירה", longTerm: false,photos:[]}, contact: {name: '', phone: 0},status:1}, 
                      size: "", rooms: "", price: "", floor: "", photos:[], neighborhoods:[],visible: true,loading:false, roomsColor: 'black', phoneColor: 'black',
                      sizeColor: 'black',neighborhoodColor: 'black', addressColor: 'black', nameColor: 'black', priceColor: 'black', isValid: true, load:false});
      })
      .catch(function(error) {
          console.error("Error adding document: ", error);
      });
  }

  uploadToFirebase(blob, path) {
      return new Promise((resolve, reject)=>{
          var storageRef = firebase.storage().ref();
          storageRef.child(path).put(blob, {
              contentType: 'image/jpeg'
          }).then((snapshot)=>{
              blob.close();
              resolve(snapshot);
          }).catch((error)=>{
              reject(error);
          });
      });
  }

  uriToBlob(uri) {
      return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.onload = function() {
          // return the blob
          resolve(xhr.response);
        };
        
        xhr.onerror = function() {
          // something went wrong
          reject(new Error('uriToBlob failed'));
        };
        // this helps us get a blob
        xhr.responseType = 'blob';
        xhr.open('GET', uri, true);
        
        xhr.send(null);
      });
  }

  handleChoosePhoto() {
      const options = {
        noData: true
      }
      ImagePicker.launchImageLibrary(options, response => {
        if (response.uri && !this.state.photos.some(photo => photo.fileName == response.fileName)) {
          this.setState(prevState => ({
              photos: [...prevState.photos, response]
            }))
        }
      })
  }

  dataList = (list) => {
      return( list.map( (x,i) => { 
          return( <Picker.Item label={x} key={i} value={x}  />)
      } ));
  }

  renderButton() {
    if (this.state.loading) {
      return <View style={{marginTop:10,padding:10}}><Spinner size="small" /></View>;
    }
    return(<Button containerStyle={{marginTop:10,padding:10}} title='שמור' onPress={this.Save.bind(this)}/>);
  }

  render () { 
    if (this.state.load) {
      return (<Spinner size="large"/>);
    }
      return (
          <ScrollView ref='_scrollView' style={{marginTop:5}}>
          <Text style={theme.title} > הוספת דירה חדשה</Text>
          {!this.state.isValid && <Text style={{fontSize: 15, alignSelf: 'center', color: 'red'}} hide={true} >אנא מלא את הפרטים החסרים</Text>}
          <View style={styles.containerStyle}>
              <Text style={styles.labelStyle}>סוג נכס</Text>
              <Picker
                  style= {styles.TextInputStyle}
                  selectedValue={this.state.userData.apartment_data.homeType}
                  onValueChange={homeType => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, homeType: homeType}}})}>
                  { this.dataList(this.homeTypes) }
              </Picker>
          </View>
          <View style={styles.firstContainer}>
            <Text style={{marginRight:5,fontSize:18,marginTop:"9%",color:this.state.roomsColor,marginLeft:5}}>מספר חדרים:</Text>
              <RNPickerSelect
              onValueChange={rooms => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, rooms: Number(rooms)}},rooms:rooms, roomsColor: "black"})} 
              value = {this.state.rooms}
              items={this.data}
              placeholder={{label:"בחר",value:null}}
              style={{inputAndroid:{color:"black"},
                      placeholder:{color: 'black',fontWeight:"bold",fontSize:18},
                      viewContainer:{width:"65%",marginTop:15,marginRight:"4%",borderBottomColor:"black",borderBottomWidth:2}}}/>
          </View>
          <View style={styles.secondContainer}>
            <Text style={{marginRight:12,width:'15%',fontSize:18,marginTop:15,color:this.state.addressColor}}>גודל:</Text>
            <Hoshi
              style={{width:"77%",marginTop:-15,borderBottomColor:"black",borderBottomWidth:2}}
              inputStyle={{textAlign :"left",marginRight:"25%",color:"black"}}
              borderColor={'#b76c94'}
              borderHeight={2}
              value={this.state.size}
              onChangeText={size => {
                if(!isNaN(size)) {
                  this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, size: Number(size)}},size:size, sizeColor: "black"})
                }
              }}
              keyboardType="numeric"
              maxLength={3}
              />
          </View>
          <View style={theme.viewStyle}>
            <Hoshi
              style={{width:"43%",marginRight:"6%",borderBottomColor:"black",borderBottomWidth:2}}
              labelStyle={{marginLeft:"70%",color:this.state.priceColor}}
              inputStyle={{textAlign :"left",marginRight:"35%",color:"black"}}
              label={'מחיר'}
              borderColor={'#b76c94'}
              borderHeight={2}
              inputPadding={16}
              value={this.state.price}
              onChangeText={price => {
                  if(!isNaN(price)) {
                    this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, price:  Number(price)}},price:price, priceColor: "black"})
                  }
                }
              } 
              keyboardType="numeric"
            />
            <Hoshi
              style={{width:"45%",marginRight:"4%",borderBottomColor:"black",borderBottomWidth:2}}
              labelStyle={{marginLeft:"70%",color:"black"}}
              inputStyle={{textAlign :"left",marginRight:"35%",color:"black"}}
              label={"קומה"}
              borderColor={'#b76c94'}
              borderHeight={2}
              inputPadding={16}
              value={this.state.floor}
              onChangeText={floor => {
                if(!isNaN(floor)) {
                  this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, floor: Number(floor)}},floor:floor})
                }
              }}
              keyboardType="numeric"
              maxLength={2}
            />
          </View>
          <View style={styles.containerStyle}>
            <Text style={{marginRight:10,width:'15%',fontSize:18,marginTop:15,color:this.state.addressColor}}>עיר:</Text>
            <Text style={{width:"78%",fontSize:18,marginTop:15,fontWeight:"bold"}}>תל אביב</Text>
          </View>
          <View style={{ flexDirection:'row',justifyContent: 'center',marginTop: 5}}>
            <AutoComplete 
            textColor = {this.state.neighborhoodColor}
            label="שכונה"
            data={this.state.neighborhoods} 
            onChange={neighborhood => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, neighborhood: neighborhood.key}}, neighborhoodColor: "black"})} />
          </View>
          <View style={styles.containerStyle}>
            <Text style={{marginRight:5,width:'18%',fontSize:18,marginTop:15,color:this.state.addressColor}}>כתובת:</Text>
            <Hoshi
              style={{width:"76%",marginTop:-15,borderBottomColor:"black",borderBottomWidth:2}}
              inputStyle={{textAlign :"right",marginRight:"25%",color:"black"}}
              borderColor={'#b76c94'}
              borderHeight={2}
              value={this.state.userData.apartment_data.address}
              onChangeText={address => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, address: address}}, addressColor: "black"})}
            />
          </View>
          <Text style={{marginTop:20, marginRight:5,fontSize: 25,color:"#4d263c",fontWeight:"bold"}} >  סמן מה יש בדירה:</Text>
          <View style={theme.detailsView}>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="מזגן"
              checked={this.state.userData.apartment_data.ac}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, ac: !this.state.userData.apartment_data.ac}}})}/>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="נגישות"
              checked={this.state.userData.apartment_data.accessible}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, accessible: !this.state.userData.apartment_data.accessible}}})}/>
          </View>
          <View style={theme.detailsView}>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="מרפסת"
              checked={this.state.userData.apartment_data.balcony}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, balcony: !this.state.userData.apartment_data.balcony}}})}/>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="מעלית"
              checked={this.state.userData.apartment_data.elevator}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, elevator: !this.state.userData.apartment_data.elevator}}})}/>
          </View>
          <View style={theme.detailsView}>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="מטבח מאובזר"
              checked={this.state.userData.apartment_data.equipped_kitchen}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, equipped_kitchen: !this.state.userData.apartment_data.equipped_kitchen}}})}/>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="מרוהטת"
              checked={this.state.userData.apartment_data.furnished}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, furnished: !this.state.userData.apartment_data.furnished}}})}/>
          </View>
          <View style={theme.detailsView}>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="חנייה"
              checked={this.state.userData.apartment_data.parking}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, parking: !this.state.userData.apartment_data.parking}}})}/>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="שותפים"
              checked={this.state.userData.apartment_data.partners}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, partners: !this.state.userData.apartment_data.partners}}})}/>
          </View>
          <View style={theme.detailsView}>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="בעלי חיים"
              checked={this.state.userData.apartment_data.pets}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, pets: !this.state.userData.apartment_data.pets}}})}/>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="מקלט"
              checked={this.state.userData.apartment_data.shelter}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, shelter: !this.state.userData.apartment_data.shelter}}})}/>
          </View>  
          <View style={theme.detailsView}>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="מחסן"
              checked={this.state.userData.apartment_data.storeroom}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, storeroom: !this.state.userData.apartment_data.storeroom}}})}/>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="משופצת"
              checked={this.state.userData.apartment_data.refurbished}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, refurbished: !this.state.userData.apartment_data.refurbished}}})}/>
          </View>
          <View style={theme.detailsView}>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="סורגים"
              checked={this.state.userData.apartment_data.bars}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, bars: !this.state.userData.apartment_data.bars}}})}/>
            <CheckBox
              iconRight
              right
              containerStyle={theme.checkBoxStyle}
              textStyle={{color:"black",fontSize:16}}
              uncheckedColor={"black"}
              checkedColor={"#732F72"}
              title="לטווח ארוך"
              checked={this.state.userData.apartment_data.longTerm}
              onPress={() => this.setState({userData:{...this.state.userData, apartment_data: {...this.state.userData.apartment_data, longTerm: !this.state.userData.apartment_data.longTerm}}})}/>
          </View>
          <View style={{marginTop:15}}>
              <Button buttonStyle={{borderRadius: 15,borderWidth:0,backgroundColor:"white"}}
                      containerStyle={{alignSelf:'flex-end',width:"50%",marginRight:25}} 
                      title="העלה תמונות של הנכס" 
                      onPress={this.handleChoosePhoto.bind(this)} />
              {this.state.photos.map(photo => {
                  return <Text style={{textAlign:"right",marginRight:25}}>{photo.fileName}</Text>;
              })}
          </View>
          <Text style={{marginTop:40, marginRight:5,fontSize: 25,color:"#4d263c",fontWeight:"bold"}} >  פרטים אישיים:</Text>
          <View style={theme.viewStyleSec}>
            <Hoshi
              style={{width:"90%",marginRight:"6%",borderBottomColor:"black",borderBottomWidth:2}}
              labelStyle={{marginLeft:"75%",color:this.state.nameColor}}
              inputStyle={{textAlign :"right",marginRight:"25%",color:"black"}}
              label={'שם לתצוגה'}
              borderColor={'#b76c94'}
              borderHeight={2}
              inputPadding={16}
              value={this.state.userData.contact.name}
              onChangeText={name => this.setState({userData: {...this.state.userData, contact: {...this.state.userData.contact, name: name}}, nameColor: "black"})}
            />
          </View>
          <View style={theme.viewStyleSec}>
            <Hoshi
              style={{width:"90%",marginRight:"6%",borderBottomColor:"black",borderBottomWidth:2}}
              labelStyle={{marginLeft:"85%",color:this.state.phoneColor}}
              inputStyle={{textAlign :"right",marginRight:"25%",color:"black"}}
              label={'טלפון'}
              borderColor={'#b76c94'}
              borderHeight={2}
              inputPadding={16}
              value={this.state.userData.contact.phone}
              onChangeText={phone => this.setState({userData: {...this.state.userData, contact: {...this.state.userData.contact, phone: phone}}, phoneColor: "black"})}
              keyboardType={'numeric'}
            />
          </View>
          {this.renderButton()}
          <SCLAlert
              theme="success"
              show={this.state.visible}
              title="הדירה נוספה בהצלחה!"
              subtitle="לחץ לחזרה לדף הדירות"
              onRequestClose={() => {this.setState({visible:false}); this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "apartments" } })}}
              >
              <SCLAlertButton theme="success" onPress={() => {this.setState({visible:false}); this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "apartments" } })}}>
                  OK
              </SCLAlertButton>
          </SCLAlert>

        </ScrollView>
      );
    }   
  }

  const styles = EStyleSheet.create({
    TextInputStyle: {
      fontSize: 16,
      width: "50%",
      borderBottomWidth:2,
      borderBottomColor:"#b9c1ca"
    },
    labelStyle: {
      fontSize: 18,
      paddingTop: 12,
      width:'35%',
      color:"black"
    },
    containerStyle: {
      flexDirection: 'row-reverse',
      alignSelf: 'center',
      marginTop: 10,
      marginBottom: 20
    },
    firstContainer:{
      flexDirection: 'row-reverse',
      alignSelf: 'center',
      marginBottom: 20,
      marginTop: -20,
    },
    secondContainer:{
      flexDirection: 'row-reverse',
      alignSelf: 'center',
      marginTop: 10,
      marginBottom: 30
    }
});

export default withNavigation(AddApartment);
