import React, {Component} from 'react';
import { ScrollView,Text, View, Image, ImageBackground } from 'react-native';
import {Spinner} from './common';
import {withNavigation} from 'react-navigation';
import { Header,CheckBox,Button,ThemeProvider } from 'react-native-elements';
import firebase from 'firebase';
import { SliderBox } from "react-native-image-slider-box";
import theme from '../themes/mainTheme';
import RatingPopup from "./RatingPopup";
import constant from '../const';
import { Icon } from 'react-native-elements';
import { SCLAlert, SCLAlertButton } from 'react-native-scl-alert';

class ApartmentDetails extends Component {
    state = { modalVisible:false, loading: false, bestApart: false,images: [
        require('../../images/placeholder.jpg')],userData:{ apartment_data: {description:"", ac: false, accessible: false, balcony: false, elevator: false,
        furnished: false, parking: false, partners: false, pets: false, shelter: false,
        storeroom: false, longTerm: false, price:"", rooms:""}, contact:{}, status:""}};

    componentDidMount(){
        this.getApartment();
    }

    componentDidUpdate(prevProp){
        if(prevProp.route.params.apartmentId != this.props.route.params.apartmentId && !this.state.loading) {
            this.getApartment();
        }   
    }

    getApartment(){
        this.setState({loading: true});
        const storageRef = firebase.storage().ref();
        const imgs = [];
        firebase.firestore().collection("apartments").doc(this.props.route.params.apartmentId).get()
        .then((querySnapshot) => {
            if(querySnapshot.data().photos){
                const urls = querySnapshot.data().photos.map(imagePath => {
                    return new Promise(resolve => {
                        resolve(storageRef.child(this.props.route.params.apartmentId + "/" + imagePath).getDownloadURL());
                    })
                });
    
                Promise.all(urls).then(result => {
                    if(result.length != 0){
                        this.setState({images: result});
                    } 
                });
            }
            
            this.setState({userData: querySnapshot.data(),loading: false});
        })
        .catch((e) => {
        console.log(e, "catch");
        });
    }

    signOut(){
        firebase.auth().signOut();
        this.props.navigation.navigate('master', { screen: 'Home' })
    }

    changeStatusApartment(newStatus){
        const saveToDB = {}
        saveToDB.status = Number(newStatus);
        const db = firebase.firestore();
        db.collection('apartments').doc(this.props.route.params.apartmentId).update(saveToDB);
        this.setState({userData: {...this.state.userData, status: newStatus}});
        this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "RatedApartments" } })

    }

    renderContent() {
        if (this.state.loading) {
            return (<Spinner size="large"/>);
        }
        return(
        <ScrollView>
            <SliderBox
            images={this.state.images}
            sliderBoxHeight={250}
            />
            <Text style={{marginTop:10,marginRight:10,fontSize:20}}>מה יש בדירה?</Text>
            <View style={theme.detailsView}>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='air-conditioner' type='material-community'/>}
                  uncheckedIcon={<Icon
                    name='air-conditioner' type='material-community' color='#AAAAA9'/>}
                title="מזגן"
                checked={this.state.userData.apartment_data.ac}/>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='accessible' type='material'/>}
                  uncheckedIcon={<Icon
                    name='accessible' type='material' color='#AAAAA9'/>}
                title="נגישות"
                checked={this.state.userData.apartment_data.accessible}/>
            </View>
            <View style={theme.detailsView}>
            <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='garage-open' type='material-community' size={25}/>}
                  uncheckedIcon={<Icon
                    name='garage-open' type='material-community' color='#AAAAA9' size={25}/>}
                title="מרפסת"
                checked={this.state.userData.apartment_data.balcony}/>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='elevator' type='foundation'/>}
                  uncheckedIcon={<Icon
                    name='elevator' type='foundation' color='#AAAAA9'/>}
                title="מעלית"
                checked={this.state.userData.apartment_data.elevator}/>
            </View>
            <View style={theme.detailsView}>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='bars' type='font-awesome'/>}
                  uncheckedIcon={<Icon
                    name='bars' type='font-awesome' color='#AAAAA9'/>}
                title="סורגים"
                checked={this.state.userData.apartment_data.bars}/>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='sofa' type='material-community'/>}
                  uncheckedIcon={<Icon
                    name='sofa' type='material-community' color='#AAAAA9'/>}
                title="מרוהטת"
                checked={this.state.userData.apartment_data.furnished}/>
            </View>
            <View style={theme.detailsView}>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='parking' type='material-community'/>}
                  uncheckedIcon={<Icon
                    name='parking' type='material-community' color='#AAAAA9'/>}
                title="חנייה"
                checked={this.state.userData.apartment_data.parking}/>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='people' type='material'/>}
                  uncheckedIcon={<Icon
                    name='people' type='material' color='#AAAAA9'/>}
                title="שותפים"
                checked={this.state.userData.apartment_data.partners}/>
            </View>
            <View style={theme.detailsView}>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='dog-side' type='material-community'/>}
                  uncheckedIcon={<Icon
                    name='dog-side' type='material-community' color='#AAAAA9'/>}
                title="בעלי חיים"
                checked={this.state.userData.apartment_data.pets}/>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='box' type='feather'/>}
                  uncheckedIcon={<Icon
                    name='box' type='feather' color='#AAAAA9'/>}
                title="מקלט"
                checked={this.state.userData.apartment_data.shelter}/>
            </View>  
            <View style={theme.detailsView}>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='garage' type='material-community'/>}
                  uncheckedIcon={<Icon
                    name='garage' type='material-community' color='#AAAAA9'/>}
                title="מחסן"
                checked={this.state.userData.apartment_data.storeroom}/>
                <CheckBox
                disabled
                iconRight
                right
                containerStyle={theme.checkBoxStyle}
                textStyle={{fontSize:16}}
                checkedIcon={<Icon
                    name='file' type='octicon'/>}
                  uncheckedIcon={<Icon
                    name='file' type='octicon' color='#AAAAA9'/>}
                title="טווח ארוך"
                checked={this.state.userData.apartment_data.longTerm}/>
            </View>
            <Text style={{marginTop:10,marginRight:10,fontSize:22,fontWeight:"bold"}}>פרוט דירה:</Text>
            <Text style={{marginTop:5,marginRight:10,fontSize:16}}>{this.state.userData.apartment_data.description}</Text>
            <View style={theme.apertView}>
                <Text style={{marginTop:10,marginRight:30,fontSize:20}}>{this.state.userData.apartment_data.address}</Text>
                <Text style={{marginTop:10,marginRight: 10,fontSize:20,fontWeight:"bold"}}>כתובת:</Text>
            </View>
            <Text style={{marginTop:10,marginRight:101,fontSize:20}}> בשכונה {this.props.route.params.neighborhood}</Text>
            <View style={theme.apertView}>
                <Text style={{marginTop:10,marginRight:125,fontSize:20}}>{this.state.userData.apartment_data.size} מ"ר </Text>
                <Text style={{marginTop:10,marginRight: 10,fontSize:20,fontWeight:"bold"}}>גודל:</Text>
            </View>
            <View style={theme.apertView}>
                <Text style={{marginTop:10,marginRight: 60,fontSize:20}}>₪{this.state.userData.apartment_data.price}</Text>
                <Text style={{marginTop:10,marginRight: 10,fontSize:20,fontWeight:"bold"}}>מחיר הדירה:</Text>
            </View>
            <View style={theme.apertView}>
                <Text style={{marginTop:10,marginRight:52,fontSize:20}}>{this.state.userData.apartment_data.rooms} חדרים </Text>
                <Text style={{marginTop:10,marginRight: 10,fontSize:20,fontWeight:"bold"}}>מספר חדרים:</Text>
            </View>
            <View style={theme.apertView}>
                <Text style={{marginTop:10,marginRight: 20,fontSize:20}}>{this.state.userData.contact.phone} ({this.state.userData.contact.name}) </Text>
                <Text style={{marginTop:10,marginRight: 10,fontSize:20,fontWeight:"bold"}}>טלפון לפרטים נוספים:</Text>
            </View>
            {this.renderButton()}
        </ScrollView>
        )
    }

    renderRatingButton(){
        if (this.props.route.params.rated){
            return (<Button containerStyle={{width:"60%",alignSelf: 'center',margin: 10}} 
            titleStyle={{color:'white'}} 
            buttonStyle={{ backgroundColor: "green",borderRadius: 15, borderWidth: 1}} 
            title="צפה בדירוג"
            onPress={() => {
                this.setState({modalVisible: true});
            }} />);
        } else {
            return (<Button containerStyle={{width:"60%",alignSelf: 'center',margin: 10}} 
            titleStyle={{color:'white'}} 
            buttonStyle={{ backgroundColor: "green",borderRadius: 15, borderWidth: 1}} 
            title="דרג דירה"
            onPress={() => {
                this.setState({modalVisible: true});
            }} />);
        }
    }

    renderButton(){
        if (this.props.route.params.isAdmin){
            if (this.state.userData.status !== "") {
                if (this.state.userData.status == constant.status.waitingForApproval){
                    return (
                        <View style={theme.apertView}>
                            <Button containerStyle={{width:"40%", margin: "5%"}} 
                                titleStyle={{color:'white'}} 
                                buttonStyle={{ backgroundColor: "green",borderRadius: 15, borderWidth: 0}} 
                                title='מאושרת'
                                onPress={()=>{this.changeStatusApartment(constant.status.approve)}}/>
                            <Button containerStyle={{width:"40%", margin: "5%"}} 
                                titleStyle={{color:'white'}} 
                                buttonStyle={{ backgroundColor: "#c71b1b" ,borderRadius: 15, borderWidth: 0}} 
                                title='לא מאושרת'
                                onPress={()=>{this.changeStatusApartment(constant.status.disApprove)}}/>
                        </View>
                    )
                } else {
                    return (
                        <View style={theme.apertView}>
                            <Text style={{marginTop:10,marginRight: 20,fontSize:20}}>{constant.statusDisplay[this.state.userData.status].display}</Text>
                            <Text style={{marginTop:10,marginRight: 10,fontSize:18}}>הדירה:</Text>
                        </View>
                    )
                }
            } else {
                return <Text> </Text>
            }
        } else {    
            return (<View>
                <RatingPopup apartmentId={this.props.route.params.myApartmentId}  
                     modalVisible={this.state.modalVisible} 
                     done={() => {this.sendRating()}} 
                     close={()=> {this.setState({modalVisible: false})}}></RatingPopup>
                {this.renderRatingButton()}
                <SCLAlert
                  theme="success"
                  show={this.state.bestApart}
                  title="זאת הדירה בשבילך!">
                    <Text style={{fontSize:20,marginTop:-40}}>על פי הנתונים שהזנת ותאריך היעד שלך, נראה שהדירה הזאת היא הדירה המתאימה בשבילך! התקשר עכשיו: {this.state.userData.contact.phone} ({this.state.userData.contact.name})</Text>
                  <SCLAlertButton theme="success" onPress={() => {this.setState({bestApart:false});}}>
                      חזור
                  </SCLAlertButton>
                </SCLAlert>
            </View>);         
        }
    }

    sendRating(){
        this.setState({modalVisible: false, loading: true})
        let userId = firebase.auth().currentUser.uid;
        let req = {"userId": userId, "matchId": this.props.route.params.myApartmentId};

        fetch('https://us-central1-diralehaskir.cloudfunctions.net/decisionAlgo', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req)
        }).then((response) => {
            response.json().then((responseJson) =>{
                console.log(responseJson);
                if(responseJson && responseJson.result){
                    this.setState({bestApart: true, loading: false});
                } else {
                    this.setState({ loading: false});
                    this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "RatedApartments" } })
                }
            })});
    }

    render () {
        return (
            <ImageBackground
            source={require('../../images/back.jpg')}
            style={{width: '100%', height: '100%',position:"absolute"}}>
                <ThemeProvider theme={theme}>
                <Header backgroundColor="transperent" containerStyle={{height: '15%',marginTop:-15}}
                    leftComponent={{ icon: 'logout', type:'simple-line-icon', color: 'black',onPress:() => this.signOut() }}
                    centerComponent={<Image source={require('../../images/diralehaskir.png')} style={{ width: 140, height: 100 }}></Image>}
                    rightComponent={{ icon: 'arrow-forward', color: 'black',onPress:() => this.props.navigation.navigate('master', { screen: 'Main',params: { screen: "apartments" } }) }}/>
                {this.renderContent()}
                </ThemeProvider>
            </ImageBackground>
        );

    }   
}

export default withNavigation(ApartmentDetails);
