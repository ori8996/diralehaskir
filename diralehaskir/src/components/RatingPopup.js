import React, {Component} from 'react';
import { Text, View, Modal, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import firebase from 'firebase';
import { AirbnbRating } from 'react-native-ratings';
import Icon from 'react-native-vector-icons/AntDesign';
import {Spinner} from './common';

class RatingPopup extends Component {
    state = {partition:5, state:5, owner:5, location:5, items:5, isRated:false,loading: false};

    componentDidMount(){
      const userId = firebase.auth().currentUser.uid;
      const db = firebase.firestore();
      console.log(userId);
      db.collection('users').doc(userId).collection("my_rated_apartments").doc(this.props.apartmentId).get()
      .then((querySnapshot) => {
        console.log(querySnapshot.data())
        if(querySnapshot.data() && querySnapshot.data().ratingApartment){
          this.setState({partition: querySnapshot.data().ratingApartment.partition,
                         state: querySnapshot.data().ratingApartment.state,
                         owner: querySnapshot.data().ratingApartment.owner,
                         location: querySnapshot.data().ratingApartment.location,
                         items: querySnapshot.data().ratingApartment.items,
                         isRated: true});
        }
    })
    .catch((e) => {
      console.log(e, "catch");
    });
    }

    ratingCompleted(rating, field) {
      this.setState({[field]: rating})
    }

    saveRating(){
      console.log("this.state: ", this.state);
      this.setState({loading: true});
      const userId = firebase.auth().currentUser.uid;
      const db = firebase.firestore();
      let saveRating = {partition : this.state.partition, state: this.state.state, owner: this.state.owner, location: this.state.location, items: this.state.items};
      const saveToDB = {"ratingApartment" : saveRating};
      db.collection('users').doc(userId).collection("my_apartments").doc(this.props.apartmentId).update(saveToDB).then(()=> {
        db.collection("users").doc(userId).get().then((querySnapshot) => {
          if(!querySnapshot.data().startTime) {
            db.collection('users').doc(userId).set({startTime: Date.now()}, {merge: true});
          }
          
          this.setState({loading: false});
          this.close();
          this.props.done();
        });
      });  
    }



    close(){
        this.props.close();
    }

    renderButton(){
      if(this.state.isRated){
        return (<View style={{marginTop:30, margin: 20, flex: 1, flexDirection: 'row'}}>
                  <Button style={{flex: 1}} buttonStyle={{ borderWidth: 0}} onPress={this.close.bind(this)} icon={
                      <Icon
                      name="closecircleo"
                      size={30}
                      color="black"
                      />
                  } type="clear"/>
                </View>);
      } else {
        return(<View style={{marginTop:30, margin: 20, flex: 1, flexDirection: 'row'}}>
                  <Button style={{flex: 1}} buttonStyle={{ borderWidth: 0}} onPress={this.saveRating.bind(this)} icon={
                      <Icon
                      name="checkcircleo"
                      size={30}
                      color="black"
                      />
                  } type="clear"/>
                  <Button style={{flex: 1}} buttonStyle={{ borderWidth: 0}} onPress={this.close.bind(this)} icon={
                      <Icon
                      name="closecircleo"
                      size={30}
                      color="black"
                      />
                  } type="clear"/>
              </View>);
      }
    }
 
    render(){
      if(this.state.loading){
        return (<Modal
          animationType="slide"
          transparent={true}
          visible={this.props.modalVisible}
          onRequestClose={() => { }}
          >
              <View style={styles.modalView}>
              <Text style={{fontSize: 20, marginBottom:20}}>הדירוג מתבצע, אנא המתן...</Text>
                <Spinner size="large"/>
              </View>
          </Modal>   
        );
      } else {
        return (
          <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.modalVisible}
          onRequestClose={() => { }}
          >
              <View>
              <View style={styles.modalView}>
                  <Text style={{fontSize: 20, marginBottom:20}}>איך הדירה בעיניך? </Text>
                  <Text style={{marginTop:20}}>גודל וחלוקת הדירה: </Text>
                  <AirbnbRating
                  count={10}
                  reviews={[]}
                  isDisabled={this.state.isRated}
                  defaultRating={this.state.partition}
                  size={24}
                  reviewSize={0}
                  onFinishRating={(rating) => (this.ratingCompleted(rating, "partition"))}
                  />
                  <Text style={{marginTop:20}}>מצב הדירה: </Text>
                  <AirbnbRating
                  count={10}
                  reviews={[]}
                  isDisabled={this.state.isRated}
                  defaultRating={this.state.state}
                  size={24}
                  reviewSize={0}
                  onFinishRating={(rating) => (this.ratingCompleted(rating, "state"))}
                  />
                  <Text style={{marginTop:20}}>בעל הדירה: </Text>
                  <AirbnbRating
                  count={10}
                  reviews={[]}
                  isDisabled={this.state.isRated}
                  defaultRating={this.state.owner}
                  size={24}
                  reviewSize={0}
                  onFinishRating={(rating) => (this.ratingCompleted(rating, "owner"))}
                  />
                  <Text style={{marginTop:20}}>סביבת הדירה: </Text>
                  <AirbnbRating
                  count={10}
                  reviews={[]}
                  isDisabled={this.state.isRated}
                  defaultRating={this.state.location}
                  size={24}
                  reviewSize={0}
                  onFinishRating={(rating) => (this.ratingCompleted(rating, "location"))}
                  />
                  <Text style={{marginTop:20}}>אבזור הדירה: </Text>
                  <AirbnbRating
                  count={10}
                  reviews={[]}
                  isDisabled={this.state.isRated}
                  defaultRating={this.state.items}
                  size={24}
                  reviewSize={0}
                  onFinishRating={(rating) => (this.ratingCompleted(rating, "items"))}
                  />
                  {this.renderButton()}
              </View>
              </View>
          </Modal>
          );
      } 
    }
}


const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
    },
    modalView: {
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding: 35,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
    },
    openButton: {
      backgroundColor: "#F194FF",
      borderRadius: 20,
      padding: 10,
      elevation: 2
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      textAlign: "center"
    }
  });

export default RatingPopup;
