import React, {Component} from 'react';
import {ScrollView, Text, View } from 'react-native';
import {Button} from 'react-native-elements';
import firebase from 'firebase';
import Apartment from './Apartment';
import {Spinner} from './common';

class Apartments extends Component {
  state = {apartments: [], loading: true, loadingMore: false, lastApart: {},neighborhoods:[]};
  unsubscribe;

  componentDidMount(){
    this.getApartments();
  }

  componentDidUpdate(prevProps, prevState){
    if (prevProps.isAdmin !== this.props.isAdmin){
      this.getApartments();
    }
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  getApartments(){
    let items = [];

    if (this.props.isAdmin){
        this.unsubscribe = firebase.firestore().collection("apartments").where("status","==",1).orderBy(firebase.firestore.FieldPath.documentId()).limit(10)
            .onSnapshot((querySnapshot) => {
                console.log("admin");
                let last = querySnapshot.docs[querySnapshot.docs.length-1];
                querySnapshot.forEach(function(doc) {
                    items.push({id: doc.id, data: doc.data()});
                });
                this.setState({apartments: items, loading: false,lastApart: last});
            });
    } else {
      const userId = firebase.auth().currentUser.uid;
      this.unsubscribe = firebase.firestore().collection("users").doc(userId).collection("my_apartments").orderBy(firebase.firestore.FieldPath.documentId()).limit(5)
        .onSnapshot((querySnapshot) => {
            console.log("user");
            let tempItems = [];
            let tempIdItems = [];
            let items = [];
            let last = querySnapshot.docs[querySnapshot.docs.length-1];

            querySnapshot.forEach(function(doc) {
              tempIdItems.push(doc.id);
              tempItems.push(new Promise(resolve => {
              resolve(doc.data().apartmentRef.get());
              }));
            });

            Promise.all(tempItems).then(result => {
            if(result.length != 0){
                items = result.map((res, index) => {
                return {myApartmentId: tempIdItems[index] ,id: res.id, data: res.data()};
                })
            }
            this.setState({apartments: items, loading: false,lastApart: last}); 
            });
        })
    }
  }

  reloadMore(){
    this.setState({loadingMore: true});
    const userId = firebase.auth().currentUser.uid;

    if (this.props.isAdmin){
      firebase.firestore().collection("apartments").where("status","==",1).orderBy(firebase.firestore.FieldPath.documentId()).startAfter(this.state.lastApart).limit(10).get()
          .then((querySnapshot) => {
            if(querySnapshot.docs.length != 0){
              let items = [];
              let last = querySnapshot.docs[querySnapshot.docs.length-1];
              querySnapshot.forEach(function(doc) {
                  items.push({id: doc.id, data: doc.data()});
              });
              this.setState({apartments: [ ...this.state.apartments, ...items ], loading: false,lastApart: last, loadingMore: false});
            } else {
              this.setState({loading: false,loadingMore: false});
            }
          });
    } else {
      firebase.firestore().collection("users").doc(userId).collection("my_apartments").orderBy(firebase.firestore.FieldPath.documentId()).startAfter(this.state.lastApart).limit(5).get()
        .then((querySnapshot) => {
          if(querySnapshot.docs.length != 0){
            let tempItems = [];
            let tempIdItems = [];
            let items = [];
            let last = querySnapshot.docs[querySnapshot.docs.length-1];

            querySnapshot.forEach(function(doc) {
              tempIdItems.push(doc.id);
              tempItems.push(new Promise(resolve => {
              resolve(doc.data().apartmentRef.get());
              }));
            });

            Promise.all(tempItems).then(result => {
            if(result.length != 0){
                items = result.map((res, index) => {
                return {myApartmentId: tempIdItems[index] ,id: res.id, data: res.data()};
                })
            }
            this.setState({apartments: [ ...this.state.apartments, ...items ], loading: false ,lastApart: last, loadingMore: false}); 
            });
          } else {
            this.setState({loading: false,loadingMore: false});
          }
            
      })
    }
  }

  onButtonPress(apartmentId, myApartmentId,neighborhood) {
    this.props.navigation.navigate('master', { screen: 'ApartmentDetails',params: { apartmentId: apartmentId, isAdmin: this.props.isAdmin, myApartmentId: myApartmentId, rated: false, neighborhood: neighborhood } }); 
  }

  renderMore(){
    if(this.state.loadingMore){
      return (<Spinner size="large"/>);
    }

    return (<Button containerStyle={{marginBottom:15}} 
      title="טען עוד"
      onPress= {this.reloadMore.bind(this)}></Button>);
  }

  renderHeader(){
    if(!this.props.isAdmin){
      return (<Text style={{textAlign:"center",fontSize:30}}>דירות מותאמות בשבילך</Text>);
    } else {
      return (<Text style={{textAlign:"center",fontSize:30}}>דירות המחכות לאישור</Text>);
    }
  }

  render() {
    if (!this.props.isAdmin && !this.state.loading && this.state.apartments.length === 0) {
      return (<View>
                <Text style={{ marginTop:5, lineHeight:30, fontSize: 20, fontStyle:"italic"}}> המערכת מחפשת עבורך את הדירות המתאימות </Text>
                <View  style={{marginTop:20}}><Spinner size="large"/></View>
              </View>);
    } else if(this.props.isAdmin && !this.state.loading && this.state.apartments.length === 0){
      return (<View>
        <Text style={{ marginTop:5, lineHeight:30, fontSize: 20, fontStyle:"italic"}}> מחפש דירות שמחכות לאישור </Text>
        <View  style={{marginTop:20}}><Spinner size="large"/></View>
      </View>);
    }

    if (this.state.loading) {
      return (<Spinner size="large"/>);
    }
    return (<ScrollView contentContainerStyle={{justifyContent:"center"}}>
      {this.renderHeader()}
      {this.state.apartments.map((apartment, i) => {
          return <Apartment apartment={apartment} neighborhoods={this.props.neighborhoods} isAdmin={this.props.isAdmin} onPress={this.onButtonPress.bind(this)} ></Apartment>
      })}
      {this.renderMore()}
    </ScrollView>)
  }
}

export default Apartments;
