import React, {Component} from 'react';
import Home from './components/Home';
import Login from './components/Login';
import AddApartment from './components/AddApartment';
import Register from './components/Register';
import Main from './components/Main';
import BasicQuestions from './components/BasicQuestions';
import Apartments  from './components/Apartments';
import RatedApartments  from './components/RatedApartments';
import ApartmentDetails from './components/ApartmentDetails';
import userDetails from './components/BasicQuestions';
import GenerateApartments from './components/GenerateApartments';
import Welcome from './components/Welcome';
import TimeSelection from './components/TimeSelection';
import ExplanationPage from './components/ExplanationPage';
import EStyleSheet from 'react-native-extended-stylesheet';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import firebase from 'firebase';
import {BackHandler} from 'react-native';

EStyleSheet.build({});
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

class App extends Component {
  componentDidMount() {
    if (firebase.apps.length === 0) {
      firebase.initializeApp({
        apiKey: 'AIzaSyBTu5dxJltQa14jlg7HPsK1M1fuMs9w30s',
        authDomain: 'diralehaskir.firebaseapp.com',
        databaseURL: 'https://diralehaskir.firebaseio.com',
        projectId: 'diralehaskir',
        storageBucket: 'diralehaskir.appspot.com',
        messagingSenderId: '22215261566',
        appId: '1:22215261566:web:0fd13da52c22d906de472f',
      });
    }
    BackHandler.addEventListener('hardwareBackPress', () => {return true});

  }
  
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', () => {return true});
  }

  master(){
    return(<Stack.Navigator screenOptions={{headerShown : false}}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Main" component={Main} />
      <Stack.Screen name="AddApartment" component={AddApartment} />
      <Stack.Screen name="Apartments" component={Apartments} />
      <Stack.Screen name="RatedApartments" component={RatedApartments} />
      <Stack.Screen name="ApartmentDetails" component={ApartmentDetails} />
      <Stack.Screen name="userDetails" component={userDetails} />
      <Stack.Screen name="GenerateApartments" component={GenerateApartments} />
      <Stack.Screen name="BasicQuestions" component={BasicQuestions} />
      <Stack.Screen name="Welcome" component={Welcome} />
      <Stack.Screen name="TimeSelection" component={TimeSelection} />
      <Stack.Screen name="ExplanationPage" component={ExplanationPage} />
      </Stack.Navigator>);
  }

  render() {
    if (firebase.apps.length == 0) {
      firebase.initializeApp({
        apiKey: 'AIzaSyBTu5dxJltQa14jlg7HPsK1M1fuMs9w30s',
        authDomain: 'diralehaskir.firebaseapp.com',
        databaseURL: 'https://diralehaskir.firebaseio.com',
        projectId: 'diralehaskir',
        storageBucket: 'diralehaskir.appspot.com',
        messagingSenderId: '22215261566',
        appId: '1:22215261566:web:0fd13da52c22d906de472f',
      });
    }
    return (
      <NavigationContainer>
        <Drawer.Navigator drawerContentOptions={{labelStyle:{color:"4d263c",fontSize:18},
                                                 activeBackgroundColor:"#EFC386"}}
                          drawerStyle={{backgroundColor:"#EFC386"}} 
                          drawerPosition="right" edgeWidth={0} 
                          screenOptions={{headerShown : false}}
                          component={(this.master)}>
          <Drawer.Screen options={{ drawerLabel: ()=>null,drawerIcon:()=>null}} 
                         name="master" 
                         component={(this.master)} initialParams={{ screen: "home" }}/>
          <Drawer.Screen options={{ drawerLabel: "הוספת דירה                                        ",unmountOnBlur:true }} 
                         name="AddApartment" 
                         component={Main} 
                         initialParams={{ screen: "addApartment" }}/>
          <Drawer.Screen options={{ drawerLabel: "דירות                                             ",unmountOnBlur:true }} 
                         name="Apartments" 
                         component={Main} 
                         initialParams={{ screen: "apartments"}}/>
          <Drawer.Screen options={{ drawerLabel: "דירות שדירגתי                                    ",unmountOnBlur:true }} 
                         name="RatingApartments" 
                         component={Main} 
                         initialParams={{ screen: "RatedApartments" }}/>
          <Drawer.Screen options={{ drawerLabel: "עריכת העדפות                                     ",unmountOnBlur:true }} 
                         name="BasicQuestions" 
                         component={Main} 
                         initialParams={{ screen: "basicQuestions" }}/>
        </Drawer.Navigator>
      </NavigationContainer>
    );
  }
}

export default App;