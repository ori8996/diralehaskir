from flask import escape
from flask import jsonify
import pandas as pd
import firebase_admin
from firebase_admin import storage
from firebase_admin import credentials
from sklearn.naive_bayes import GaussianNB
import pickle
from pandas.io.json import json_normalize

cred = credentials.Certificate('./diralehaskir-firebase-adminsdk-ezfsn-96aa25b5b2.json')
firebase_admin.initialize_app(cred, {'storageBucket': "diralehaskir.appspot.com"})

def buildModel(request):
    try:
        request_json = request.get_json(silent=True)
        request_args = request.args
        if request_json and request_args and 'userId' in request_args:
            df = json_normalize(request_json)
            nb_clf= GaussianNB()
            X, Y = generateTagedApartments(df)
            X=df.drop('tag', axis=1)
            Y=df['tag']
            nb_clf.fit(X, Y)
            writeToStorage(nb_clf, request_args['userId'])
            return 'model built'.format()
        else:
            return "bad args, probably you didn't send userId on request args".format()
    except Exception as e:
        return str(e)

def generateTagedApartments(df):
    generatedDf = pd.concat([generateApartments(df[df['tag'] == 1]),generateApartments(df[df['tag'] == 0])])
    return generatedDf.iloc[:, :3],generatedDf.iloc[:, 3]


def generateApartments(df):
    newDf = pd.DataFrame(columns=['modelData.location', 'modelData.price', 'modelData.quality', 'tag'])
    for i in range(100):
        newDf = newDf.append(df.sample(n = 2).mean(),ignore_index=True)
    return pd.concat([df, newDf])

def writeToStorage(model, userId):
    bucket = storage.bucket()
    blob = bucket.blob(userId+"/model.pkl")
    blob.upload_from_string(pickle.dumps(model))

def predictMatch(request):
    try:
        request_json = request.get_json(silent=True)
        if request_json:
            ans = {}
            for user in request_json:
                userId = user["userId"]
                df = json_normalize(user["instances"])
                bucket = storage.bucket()
                blob = bucket.blob(userId + "/model.pkl")
                pkl = blob.download_as_string()
                model = pickle.loads(pkl)
                ans[userId] = model.predict(df).__str__()
            return jsonify(ans)
        else:
            return "no json received".format()
    except Exception as e:
        return str(e)