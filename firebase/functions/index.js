const functions = require('firebase-functions');
const admin = require('firebase-admin');
const axios = require('axios');
const online = require('./code/onlineAlgo');
const generator = require('./code/apartmentGenerator');
const neighborhoodsMng = require('./code/neighborhoodsMng');
const quality = require('./code/qualityMatcher');
const decision = require('./code/decisionAlgo');
admin.initializeApp();

exports.generateApartments = functions.https.onRequest(async (req, res) => {
    let apartment_prefernces = req.body.apartment_prefernces;
    apartment_prefernces["neighborhood"] = admin.firestore().doc(apartment_prefernces["neighborhood"])
    res.send(await generateApartments(apartment_prefernces));
});

exports.decisionAlgo = functions.https.onRequest(async (req, res) => {
    let snap = await admin.firestore().doc("users/" + req.body.userId).get();
    let aprtRatingSnap = await admin.firestore().collection("users/" + req.body.userId + "/my_apartments").doc(req.body.matchId).get();
    let rankedApartSnapshot = await admin.firestore().collection("users").doc(req.body.userId).collection("my_rated_apartments").get();
    let rankedAparts = [];
    rankedApartSnapshot.forEach(doc => {
        rankedAparts.push({'rank': doc.data().rank});
    });
    let maxUserQualityRank = quality.maxQuality(snap.get("apartment_prefernces"));
    let currQualityRank = quality.calcQualityForUser(snap.get("apartment_prefernces"), (await aprtRatingSnap.get("apartmentRef").get()).get("apartment_data"));
    let currPrice = (await aprtRatingSnap.get("apartmentRef").get()).get("apartment_data").price;
    let userPrice = snap.get("apartment_prefernces").price;
    let returnedObj = await decision.decision(snap.get("startTime"), snap.get("timeSelection"), snap.get("apartment_rating"), aprtRatingSnap.get("ratingApartment"), 
    rankedAparts, currQualityRank, maxUserQualityRank, currPrice, userPrice);
    let querySnapshot = await admin.firestore().collection('users').doc(req.body.userId).collection("my_apartments").doc(req.body.matchId).get();
    let rankedApart = querySnapshot.data();
    rankedApart["rank"] = returnedObj.rank;
    await admin.firestore().collection('users').doc(req.body.userId).collection("my_rated_apartments").add(rankedApart)
    await admin.firestore().collection('users').doc(req.body.userId).collection("my_apartments").doc(req.body.matchId).delete();
    console.log(returnedObj.result);
    res.send({"result": returnedObj.result});
});

async function generateApartments(apartmentPrefernces) {
    return Promise.all((await generator.generateApartments(apartmentPrefernces, 7)).map(async (apartment) => { 
        return {
            apartment : apartment,
            modelData : await calcModelData(apartmentPrefernces, apartment)
        }
    }));
}

exports.matchApartmentToUsers = functions.firestore
    .document('apartments/{apartmentId}')
    .onUpdate(async (change, context) => {
        if(change.after.get("status") === 2 && change.before.get("status") === 1) {
            return matchApartmentToUsers(change.after).then(() => {
                return "ok";
            });
        } else {
            return null;
        }
    });

async function matchApartmentToUsers(apartmentSnap) {
    console.log("apartment" + apartmentSnap.id);
    let usersSnap = (await admin.firestore().collection("users").get()).docs
    let predictQuery = [];
    let userRefs = [];
    /* eslint-disable no-await-in-loop */
    for(const userSnap of usersSnap) {
        let apartmentsCol = await userSnap.ref.collection("my_apartments").get()
        if(apartmentsCol.docs.length > 0 && apartmentSnap.get("apartment_data.rooms") >= userSnap.get("apartment_prefernces.rooms") - 1 && apartmentSnap.get("apartment_data.rooms") <= userSnap.get("apartment_prefernces.rooms") + 1 &&
        apartmentSnap.get("apartment_data.size") >= userSnap.get("apartment_prefernces.size") - 20 && apartmentSnap.get("apartment_data.size") <= userSnap.get("apartment_prefernces.size") + 20 && 
        apartmentSnap.get("apartment_data.price") >= userSnap.get("apartment_prefernces.price") - 2000 && apartmentSnap.get("apartment_data.price") <= userSnap.get("apartment_prefernces.price") + 2000) {
            predictQuery.push({
                userId:userSnap.id,
                instances: [await calcModelData(userSnap.get("apartment_prefernces"), apartmentSnap.get("apartment_data"))]
            });
            userRefs.push(userSnap.ref);
        }
    }
    /* eslint-enable no-await-in-loop */
    console.log(predictQuery);
    let res = (await predictMatch(predictQuery)).data
    console.log(res);
    addPromises = []
    for (const [index, [, value]] of Object.entries(Object.entries(res))) {
        const predictions = value;
        let numbers = Array.from(predictions.replace("[","").replace("]","").replace(/ /g, "").replace("\\n", ""));
        numbers.forEach((number, i) => {
            if(number === "1") {
                console.log(userRefs[index].path);
                addPromises.push(userRefs[index].collection("/my_apartments").add({
                    apartmentRef: apartmentSnap.ref,
                    status: "matched",
                    modelData: predictQuery[index].instances[i]
                }));
            }
        });
    }
    return addPromises;
}

exports.matchUserToApartments = functions.storage.object().onFinalize(async (object) => {
    if(object.name.split("/")[1] === "model.pkl") {
        return matchUserToApartments(object.name.split("/")[0]);
    }
});

async function matchUserToApartments(userId) {
    let userRef = admin.firestore().doc("users/" + userId);
    let predictQuery = [];
    predictQuery.push({
        userId,
        instances: []
    });
    apartmentRefs = [];
    apartmentRefs.push({
        userId,
        refs: []
    });
    let apartmentPrefernces = (await userRef.get()).get("apartment_prefernces");
    let apartmentsSnap = (await admin.firestore().collection("apartments").where("status", "==", 2).get()).docs;
    /* eslint-disable no-await-in-loop */
    let i = 0;
    for(const apartmentSnap of apartmentsSnap) {
        if(apartmentSnap.get("apartment_data.rooms") >= apartmentPrefernces.rooms - 1 && apartmentSnap.get("apartment_data.rooms") <= apartmentPrefernces.rooms + 1 &&
            apartmentSnap.get("apartment_data.size") >= apartmentPrefernces.size - 20 && apartmentSnap.get("apartment_data.size") <= apartmentPrefernces.size + 20 && 
            apartmentSnap.get("apartment_data.price") >= apartmentPrefernces.price - 2000 && apartmentSnap.get("apartment_data.price") <= apartmentPrefernces.price + 2000) {
                predictQuery[0].instances.push(await calcModelData(apartmentPrefernces, apartmentSnap.get("apartment_data")));
                apartmentRefs[0].refs.push(apartmentSnap.ref);
            }
    }
    /* eslint-enable no-await-in-loop */
    let res = (await predictMatch(predictQuery)).data;
    console.log(res);
    let predictions = Array.from(res[Object.keys(res)[0]].replace("[","").replace("]","").replace(/ /g, ""));
    let myApartmentsRef = userRef.collection("/my_apartments");
    predictions.forEach((predict, i) => {
        if(predict === "1" && apartmentRefs[0].refs[i] !== undefined) {
            myApartmentsRef.add({
                apartmentRef: apartmentRefs[0].refs[i],
                status: "matched",
                modelData: predictQuery[0].instances[i]
            });
        }
    });
}

async function predictMatch(predictQuery) {
    return axios.post('https://us-central1-diralehaskir.cloudfunctions.net/predictMatch', JSON.stringify(predictQuery), {
        headers: { 
            'Content-Type' : 'application/json' 
        }
    });
}

async function calcModelData(apartmentPrefernces, apartment) {
    return {
        price: apartment.price,
        location: await neighborhoodsMng.getDistance(apartmentPrefernces.neighborhood, apartment.neighborhood),
        quality: quality.calcQualityForUser(apartmentPrefernces, apartment)
    };
}