const hood = require('./neighborhoodsMng');
const priceGenerator = require('./priceGenerator');

exports.generateApartments = async (apartmentPrefernces, quantity) => {
    let generatedApartments = []
    let closeNeigborhoods = await hood.calcCloseNeigborhoods(apartmentPrefernces["neighborhood"]);
    /* eslint-disable no-await-in-loop */
    for(let i = 0; i < quantity ; i++) {
        generatedApartments.push(await generateApartment(apartmentPrefernces, closeNeigborhoods))
    }
    /* eslint-enable no-await-in-loop */
    return generatedApartments;
}

async function generateApartment(apartmentPrefernces, closeNeigborhoods) {
    let generatedApartment = {}
    for (const key in apartmentPrefernces) {
        if(key === "rooms") {
            generatedApartment[key] = apartmentPrefernces[key] + ((Math.round(Math.random() * 4)) / 2 - 1)
            if(generatedApartment[key] < 1) {
                generatedApartment[key] = 1;
            } else if (generatedApartment[key] > 14) {
                generatedApartment[key] = 14;
            }
        } else if(key === "neighborhood") {
            if(Math.random() > 0.5) {
                generatedApartment[key] = closeNeigborhoods[Math.floor(Math.random() * closeNeigborhoods.length)];
            } else {
                generatedApartment[key] = apartmentPrefernces[key];
            }
        } else if(key === "homeType") {
            generatedApartment[key] = (apartmentPrefernces[key])[Math.floor(Math.random() * apartmentPrefernces[key].length)];
        } else if(typeof(apartmentPrefernces[key]) === "boolean") {
            generatedApartment[key] = generateBoolean(apartmentPrefernces[key])
        } else if(typeof(apartmentPrefernces[key]) === "number") {
            generatedApartment[key] = generateNumber(apartmentPrefernces[key])
        }
    }
    generatedApartment["price"] = await priceGenerator.generatePrice(generatedApartment);
    generatedApartment["neighborhood"] = generatedApartment["neighborhood"].path;
    return generatedApartment;
}

function generateBoolean(boolean) {
    if(Math.random() < 0.7){
        return boolean;
    } else {
        return !boolean;
    }
}

function generateNumber(number) {
    return Math.round(0.8 * number + Math.random() * 0.4 * number);
}