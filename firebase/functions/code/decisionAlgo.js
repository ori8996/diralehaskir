const startingRank = 88;
const endingRank = 50;
const minRankedApartments = 3;
const higherPercentRequired = 0.79;

exports.decision = async (startTime, endTime, userApartmentRating, curruntApartmentRating, rankedApartments, currQuality, maxQuality, currPrice, userPrice) => {
    let returnedObj = {};
    let fullTime = Math.floor((endTime - startTime) / 86400000);
    let passedTime = Math.floor((Date.now() - startTime) / 86400000);
    let requiredRank = startingRank - ((startingRank - endingRank) * (passedTime / fullTime));

    let summedRationalRanking = 0;
    let summedUserAprtRanking = 0;

    for (const key in userApartmentRating) {
        summedUserAprtRanking += userApartmentRating[key];
        summedRationalRanking += userApartmentRating[key] * (curruntApartmentRating[key] / 10);
    }

    if(currQuality > maxQuality) {
        currQuality = maxQuality;
    }

    let maxPrice = userPrice + (userPrice * 0.3);
    let minPrice = userPrice - (userPrice * 0.1);
    let rangePrice = maxPrice - minPrice;
    if(currPrice < minPrice) {
        currPrice = minPrice;
    }
    if(currPrice > maxPrice) {
        currPrice = maxPrice;
    }
    let rationalPrice = 1 - ((currPrice - minPrice) / rangePrice);
    console.log("quality rank:");
    console.log((currQuality / maxQuality) * 15);
    console.log("price rank:");
    console.log(rationalPrice * 15);

    let bonusRank = rank = ((70 / summedUserAprtRanking) * summedRationalRanking) + ((currQuality / maxQuality) * 15) + (rationalPrice * 15);
    if(rankedApartments.length >= minRankedApartments) {
        let numOfRanksLowerFromCurr = 0;
        for (const currApartment in rankedApartments) {
            if(rank > currApartment["rank"]) {
                numOfRanksLowerFromCurr++;
            }
        }

        let currRankHigherPercent = numOfRanksLowerFromCurr / rankedApartments.length;
        if(currRankHigherPercent > higherPercentRequired) {
            bonusRank += rankedApartments.length * ((currRankHigherPercent - requiredRank) / (1 - higherPercentRequired))
        }
    } 

    returnedObj.rank = rank;
    returnedObj.result = (bonusRank >= requiredRank);
    console.log(rank)
    console.log(bonusRank)
    console.log(requiredRank)
    console.log(returnedObj.result);
    return returnedObj;
}