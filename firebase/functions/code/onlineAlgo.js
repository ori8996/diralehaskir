exports.apartmentProfitabillityCalculator = (ratings, modelData) => {
    return 0.5;
}

exports.searchingProcessRemainingTime = (startDate, endDate) => {
    let currentDate = startDate;
    return (endDate - currentDate) / (endDate - startDate);
}

exports.isGoodDeal = (apartmentProfitabillity, searchingProcessRemainingTime) => {
    return apartmentProfitabillity > searchingProcessRemainingTime;
}