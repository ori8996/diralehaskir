exports.calcQualityForUser = (apartmentPrefernces, apartment) => {
    let quality = 0;

    quality += (apartment.rooms - (apartmentPrefernces.rooms - 1)) * 20;
    quality += (apartment.size - (apartmentPrefernces.size - 20)) * 1.5;


    if(apartment.ac) {
        quality += 12;
        if(apartmentPrefernces.ac) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.ac) {
            quality += 10;
        }
    }
    if(apartment.accessible) {
        quality += 12;
        if(apartmentPrefernces.ac) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.ac) {
            quality += 10;
        }
    }
    if(apartment.balcony) {
        quality += 12;
        if(apartmentPrefernces.balcony) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.balcony) {
            quality += 10;
        }
    }
    if(apartment.bars) {
        quality += 12;
        if(apartmentPrefernces.bars) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.bars) {
            quality += 10;
        }
    }
    if(apartment.elevator) {
        quality += 12;
        if(apartmentPrefernces.elevator) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.elevator) {
            quality += 10;
        }
    }
    if(apartment.equipped_kitchen) {
        quality += 10;
        if(apartmentPrefernces.equipped_kitchen) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.equipped_kitchen) {
            quality += 8;
        }
    }
    if(apartment.furnished) {
        quality += 12;
        if(apartmentPrefernces.furnished) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.furnished) {
            quality += 10;
        }
    }
    if(apartment.longTerm) {
        quality += 12;
        if(apartmentPrefernces.longTerm) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.longTerm) {
            quality += 10;
        }
    }
    if(apartment.parking) {
        quality += 12;
        if(apartmentPrefernces.parking) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.parking) {
            quality += 10;
        }
    }
    if(apartment.partners) {
        quality += 10;
        if(apartmentPrefernces.partners) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.partners) {
            quality += 8;
        }
    }
    if(apartment.pets) {
        quality += 12;
        if(apartmentPrefernces.pets) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.pets) {
            quality += 10;
        }
    }
    if(apartment.refurbished) {
        quality += 10;
        if(apartmentPrefernces.refurbished) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.refurbished) {
            quality += 8;
        }
    }
    if(apartment.shelter) {
        quality += 12;
        if(apartmentPrefernces.shelter) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.shelter) {
            quality += 10;
        }
    }
    if(apartment.storeroom) {
        quality += 12;
        if(apartmentPrefernces.storeroom) {
            quality += 5;
        }
    } else {
        if(!apartmentPrefernces.storeroom) {
            quality += 10;
        }
    }

    switch(apartment.homeType) {
        case "דירה":
            quality += 10;
            break;
        case "דירת גן":
            quality += 18;
            break;
        case "דופלקס":
            quality += 28;
            break;
        case "קוטג'":
            quality += 38;
            break;
        case "פנטהאוז":
            quality += 50;
            break;
    }

    if(!apartment.accessible && apartmentPrefernces.accessible) {
        quality -= 20;
    }
    if(!apartment.pets && apartmentPrefernces.pets) {
        quality -= 15;
    }

    if(quality < 0) {
        quality = 0;
    }

    return quality;
}

exports.maxQuality = (apartmentPrefernces) => {
    let quality = 95;
    if(apartmentPrefernces.ac) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.accessible) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.balcony) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.bars) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.elevator) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.equipped_kitchen) {
        quality += 15;
    } else {
        quality += 10;
    }
    if(apartmentPrefernces.furnished) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.longTerm) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.parking) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.partners) {
        quality += 15;
    } else {
        quality += 10;
    }
    if(apartmentPrefernces.pets) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.refurbished) {
        quality += 15;
    } else {
        quality += 10;
    }
    if(apartmentPrefernces.shelter) {
        quality += 17;
    } else {
        quality += 12;
    }
    if(apartmentPrefernces.storeroom) {
        quality += 17;
    } else {
        quality += 12;
    }

    return quality;
}