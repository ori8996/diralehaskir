const admin = require('firebase-admin');

exports.generatePrice = (apartmentPrefernces) => {
    let currSize = parseInt(apartmentPrefernces.size);
    let avgRooms = parseInt(apartmentPrefernces.rooms);
    let firstAprt;
    let secAprt;
    let isInTheMiddle = false;
    console.log("aaa");
    console.log(apartmentPrefernces.neighborhood);
    let sameNeighborhoodQuery = admin.firestore().collection('apartments').
                                  where('apartment_data.neighborhood', '==', apartmentPrefernces.neighborhood).
                                  where('status', 'in', [2, 4]);
    let relevantApartments = [];
    return sameNeighborhoodQuery.get().then(snapshot => {
    if (snapshot.empty) {
      console.log('No matching documents.');
      return 0;
    }
    snapshot.forEach(doc => {
      relevantApartments.push(doc.data());
    });
    
    relevantApartments = relevantApartments.sort((a, b) => a.apartment_data.size - b.apartment_data.size);
    for (let index = 0; index < relevantApartments.length; index++) {
      const element = relevantApartments[index].apartment_data;
      if(element.size > currSize) {
        if(index === 0) {
          firstAprt = relevantApartments[index].apartment_data;
          if(relevantApartments.length > 1)
          {
            secAprt = relevantApartments[relevantApartments.length - 1].apartment_data;
          }
        }
        else {
          firstAprt = relevantApartments[index - 1].apartment_data;
          secAprt = relevantApartments[index].apartment_data;
          isInTheMiddle = true;
        }
        break;
      }
    }
    if(!firstAprt) {
      if(relevantApartments.length > 1) {
        firstAprt = relevantApartments[0].apartment_data;
        secAprt = relevantApartments[relevantApartments.length - 1].apartment_data;
      } else if (relevantApartments.length === 1) {
        firstAprt = relevantApartments[0].apartment_data;
      }
    }
    let newMoney;
    if(firstAprt) {
      if(isInTheMiddle) {
        console.log("firstAprt");
        console.log(firstAprt);
        console.log("second");
        console.log(secAprt);
        let dist = secAprt.size - firstAprt.size;
        let currDist = currSize - firstAprt.size;
        let closePercent = currDist / dist;
        if(firstAprt.price > secAprt.price) {
          closePercent = 1 - closePercent;
        }
        let moneyToAdd = parseInt(Math.abs(secAprt.price - firstAprt.price) * closePercent, 10);
        newMoney = Math.min(secAprt.price, firstAprt.price) + moneyToAdd;
        console.log("money");
        console.log(newMoney);
        if(avgRooms > firstAprt.rooms && avgRooms > secAprt.rooms) {
          newMoney += parseInt((avgRooms - Math.max(firstAprt.rooms, secAprt.rooms)) * 200, 10);
        } else if(avgRooms < firstAprt.rooms && avgRooms < secAprt.rooms) {
          newMoney -= parseInt((Math.min(firstAprt.rooms, secAprt.rooms) - avgRooms) * 200, 10);
        }
        console.log(newMoney);
        if(apartmentPrefernces.balcony && !firstAprt.balcony && !secAprt.balcony) {
          newMoney += 200;
        }
        else if(!apartmentPrefernces.balcony && firstAprt.balcony && secAprt.balcony) {
          newMoney -= 200;
        }
        if(apartmentPrefernces.parking && !firstAprt.parking && !secAprt.parking) {
          newMoney += 150;
        }
        else if(!apartmentPrefernces.parking && firstAprt.parking && secAprt.parking) {
          newMoney -= 150;
        }
        if(apartmentPrefernces.storeroom && !firstAprt.storeroom && !secAprt.storeroom) {
          newMoney += 150;
        }
        else if(!apartmentPrefernces.storeroom && firstAprt.storeroom && secAprt.storeroom) {
          newMoney -= 150;
        }
        if(apartmentPrefernces.shelter && !firstAprt.shelter && !secAprt.shelter) {
          newMoney += 100;
        }
        else if(!apartmentPrefernces.shelter && firstAprt.shelter && secAprt.shelter) {
          newMoney -= 100;
        }
        if(apartmentPrefernces.refurbished && !firstAprt.refurbished && !secAprt.refurbished) {
          newMoney += 150;
        }
        else if(!apartmentPrefernces.refurbished && firstAprt.refurbished && secAprt.refurbished) {
          newMoney -= 150;
        }
        if(apartmentPrefernces.furnished && !firstAprt.furnished && !secAprt.furnished) {
          newMoney += 100;
        }
        else if(!apartmentPrefernces.furnished && firstAprt.furnished && secAprt.furnished) {
          newMoney -= 100;
        }
      }
      else {
        if(secAprt) {
          let sizeDist = secAprt.size - firstAprt.size;
          let priceDist = secAprt.price - firstAprt.price;
          if(sizeDist !== 0) {
            let relativePrice = priceDist / sizeDist;
            let moneyToAdd = parseInt((currSize - firstAprt.size) * relativePrice, 10);
            newMoney =  Math.min(secAprt.price, firstAprt.price) + moneyToAdd;
          }
          else {
            newMoney = parseInt((secAprt.price + firstAprt.price) / 2, 10);
          }
          let closestAprt;
          if(currSize > secAprt.size){
            closestAprt = secAprt;
          } else {
            closestAprt = firstAprt;
          }
          newMoney += parseInt((avgRooms - closestAprt.rooms) * 100, 10);
          
          if(apartmentPrefernces.balcony && !closestAprt.balcony) {
            newMoney += 100;
          }
          else if(!apartmentPrefernces.balcony && closestAprt.balcony) {
            newMoney -= 100;
          }
          if(apartmentPrefernces.parking && !closestAprt.parking) {
            newMoney += 75;
          }
          else if(!apartmentPrefernces.parking && closestAprt.parking) {
            newMoney -= 75;
          }
          if(apartmentPrefernces.storeroom && !closestAprt.storeroom) {
            newMoney += 75;
          }
          else if(!apartmentPrefernces.storeroom && closestAprt.storeroom) {
            newMoney -= 75;
          }
          if(apartmentPrefernces.shelter && !closestAprt.shelter) {
            newMoney += 50;
          }
          else if(!apartmentPrefernces.shelter && closestAprt.shelter) {
            newMoney -= 50;
          }
          if(apartmentPrefernces.refurbished && !closestAprt.refurbished) {
            newMoney += 75;
          }
          else if(!apartmentPrefernces.refurbished && closestAprt.refurbished) {
            newMoney -= 75;
          }
          if(apartmentPrefernces.furnished && !closestAprt.furnished) {
            newMoney += 50;
          }
          else if(!apartmentPrefernces.furnished && closestAprt.furnished) {
            newMoney -= 50;
          }
        }
        else {
          newMoney = firstAprt.price;
        }
      }
    }
    
    if(apartmentPrefernces.homeType === "2") {
        newMoney = newMoney * 1.03;
        console.log("homeType2 " + newMoney);
    }
    if(apartmentPrefernces.homeType === "4") {
        newMoney = newMoney * 1.1;
        console.log("homeType4");
    }
    if(apartmentPrefernces.homeType === "3") {
        newMoney = newMoney * 1.08;
        console.log("homeType3");
    }
    if(apartmentPrefernces.homeType === "5") {
        newMoney = newMoney * 1.15;
        console.log("homeType5");
    }

    return newMoney - (newMoney % 50);
  })
  .catch(err => {
    console.log('Error getting documents', err);
  });

  }