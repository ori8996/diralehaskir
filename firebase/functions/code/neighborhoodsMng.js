const admin = require('firebase-admin');

exports.calcCloseNeigborhoods = async (neigborhoodRef) => {
    return admin.firestore().collection("neighborhoods").get()
      .then((querySnapshot) => {
        let closeNeigborhoods = querySnapshot.docs
        closeNeigborhoods.sort((a,b) => getDistance(a, neigborhoodRef) - getDistance(b, neigborhoodRef))
        closeNeigborhoods.splice(closeNeigborhoods.length / 5);
        return closeNeigborhoods.map(snap => snap.ref)
      })
      .catch((e) => {
        console.log(e, "catch");
    });
}
exports.getDistance = getDistance;

async function calcNeigborhoodsDistance(aRef, bRef) {
    let a = await aRef.get();
    let b = await bRef.get();
    return Math.sqrt(Math.pow(a.get("lat") - b.get("lat"), 2) + Math.pow(a.get("lon") - b.get("lon"), 2))
}

let cache = {};

async function getDistance(firstNeigborhoodRefOrSnap, secondNeigborhoodRefOrPath) {
    let firstNeigborhoodRef;
    if(firstNeigborhoodRefOrSnap.exists !== undefined) {
        firstNeigborhoodRef = firstNeigborhoodRefOrSnap.ref;
    } else {
        firstNeigborhoodRef = firstNeigborhoodRefOrSnap;
    }
    let secondNeigborhoodRef;
    if(typeof(secondNeigborhoodRefOrPath) === "string") {
        secondNeigborhoodRef = admin.firestore().doc(secondNeigborhoodRefOrPath);
    } else {
        secondNeigborhoodRef = secondNeigborhoodRefOrPath;
    }
    if(!Object.prototype.hasOwnProperty.call(cache, firstNeigborhoodRef.path)) {
        cache[firstNeigborhoodRef.path] = {}
    }
    if(!Object.prototype.hasOwnProperty.call(cache[firstNeigborhoodRef.path],(secondNeigborhoodRef.path))) {
        cache[firstNeigborhoodRef.path][secondNeigborhoodRef.path] = await calcNeigborhoodsDistance(firstNeigborhoodRef, secondNeigborhoodRef)
    }
    return cache[firstNeigborhoodRef.path][secondNeigborhoodRef.path];
}